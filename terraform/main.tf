variable "domains" {
	default = ["economysim.org", "patentwarz.com"]
	type = "list"
}

locals {
	api_dist_origin_id = "LAMBDA-api"
	static_dist_origin_id = "S3-static"
}

resource "aws_api_gateway_rest_api" "api" {
	name = "econsim-gateway"
	description = "EconSim Gateway"
}

resource "aws_api_gateway_resource" "api" {
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	parent_id = "${aws_api_gateway_rest_api.api.root_resource_id}"
	path_part = "{proxy+}"
}

resource "aws_api_gateway_method" "api" {
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	resource_id = "${aws_api_gateway_resource.api.id}"
	http_method = "ANY"
	authorization = "COGNITO_USER_POOLS"
	authorizer_id = "${aws_api_gateway_authorizer.api.id}"
}

resource "aws_api_gateway_integration" "api" {
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	resource_id = "${aws_api_gateway_method.api.resource_id}"
	http_method = "${aws_api_gateway_method.api.http_method}"
	integration_http_method = "POST"
	type = "AWS_PROXY"
	uri = "${aws_lambda_function.api.invoke_arn}"
}

resource "aws_api_gateway_authorizer" "api" {
	name = "cognito"
	type = "COGNITO_USER_POOLS"
	authorizer_result_ttl_in_seconds = 0
	provider_arns = ["${aws_cognito_user_pool.pool.arn}"]
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
}

resource "aws_api_gateway_deployment" "api" {
	depends_on = ["aws_api_gateway_method.api", "aws_api_gateway_integration.api", "aws_api_gateway_authorizer.api"]
	rest_api_id = "${aws_api_gateway_rest_api.api.id}"
	stage_name = "prod"
}

resource "aws_s3_bucket" "static" {
	acl = "private"
	bucket = "econsim-static"
	cors_rule {
		allowed_headers = ["*"]
		allowed_methods = ["GET"]
		allowed_origins = [
			for domain in var.domains:
				"https://${domain}"
		]
	}
}

data "aws_iam_policy_document" "static" {
	statement {
		sid = "cloudfront"
		actions = ["s3:GetObject"]
		resources = ["${aws_s3_bucket.static.arn}/*"]

		principals {
			type = "AWS"
			#type = "CanonicalUser"
			#identifiers = ["${aws_cloudfront_origin_access_identity.origin_access_identity.s3_canonical_user_id}"]
			identifiers = ["${replace(aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn, " ", "_")}"]
		}
	}
}

resource "aws_s3_bucket_policy" "static" {
	depends_on = ["aws_cloudfront_origin_access_identity.origin_access_identity"]
	bucket = "${aws_s3_bucket.static.id}"
	policy = "${data.aws_iam_policy_document.static.json}"
}

resource "aws_s3_bucket_public_access_block" "static" {
	bucket = "${aws_s3_bucket.static.id}"
	block_public_acls   = true
	block_public_policy = true
	ignore_public_acls = true
	restrict_public_buckets = true
}

resource "aws_s3_bucket_object" "bundle" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "application/javascript"
	cache_control = "no-cache"
	etag = "${filemd5("../static/build/client.min.js")}"
	key = "client.min.js"
	source = "../static/build/client.min.js"
}

resource "aws_s3_bucket_object" "map" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "application/javascript"
	cache_control = "no-cache"
	etag = "${filemd5("../static/build/client.min.js.map")}"
	key = "client.min.js.map"
	source = "../static/build/client.min.js.map"
}

/*
resource "aws_s3_bucket_object" "datepicker" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "text/css"
	etag = "${filemd5("../static/src/css/react-datepicker.css")}"
	key = "react-datepicker.css"
	source = "../static/src/css/react-datepicker.css"
}
*/

resource "aws_s3_bucket_object" "favicon" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "image/png"
	etag = "${filemd5("../static/src/images/Favicon.png")}"
	key = "favicon.png"
	source = "../static/src/images/Favicon.png"
}

resource "aws_s3_bucket_object" "index" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "text/html"
	etag = "${filemd5("../static/src/html/index.html")}"
	key = "index.html"
	source = "../static/src/html/index.html"
}

resource "aws_s3_bucket_object" "react-production" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "application/javascript"
	etag = "${filemd5("../static/src/umd/react-production.min.js")}"
	key = "react-production.min.js"
	source = "../static/src/umd/react-production.min.js"
}

resource "aws_s3_bucket_object" "react-dom-production" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "application/javascript"
	etag = "${filemd5("../static/src/umd/react-dom-production.min.js")}"
	key = "react-dom-production.min.js"
	source = "../static/src/umd/react-dom-production.min.js"
}

resource "aws_s3_bucket_object" "amazon-cognito-sdk" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "application/javascript"
	etag = "${filemd5("../static/lib/amazon-cognito-identity.min.js")}"
	key = "amazon-cognito-identity.min.js"
	source = "../static/lib/amazon-cognito-identity.min.js"
}

resource "aws_s3_bucket_object" "cognito" {
	bucket = "${aws_s3_bucket.static.bucket}"
	content_type = "application/json"
	key = "cognito.json"
	cache_control = "no-cache"
	content = jsonencode({
		userPoolId = "${aws_cognito_user_pool.pool.id}"
		userPoolClientId = "${aws_cognito_user_pool_client.pool.id}"
	})
}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
	comment = "Econsim"
}

resource "aws_cloudfront_distribution" "distributions" {
	count = "${length(var.domains)}"
	depends_on = ["aws_acm_certificate_validation.certs"]
	aliases = ["${element(var.domains, count.index)}", "www.${element(var.domains, count.index)}"]
	default_root_object = "index.html"
	enabled = true
	is_ipv6_enabled = true
	price_class = "PriceClass_100"
	origin {
		domain_name = "${element(split("/", aws_api_gateway_deployment.api.invoke_url), 2)}"
		origin_id = "${local.api_dist_origin_id}"
		origin_path = "/prod"
		custom_origin_config {
			http_port = 80
			https_port = 443
			origin_protocol_policy = "https-only"
			origin_ssl_protocols = ["TLSv1.2"]
		}
	}
	origin {
		domain_name = "${aws_s3_bucket.static.bucket_regional_domain_name}"
		origin_id = "${local.static_dist_origin_id}"
		s3_origin_config {
			origin_access_identity = "${aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path}"
		}
	}
	default_cache_behavior {
		allowed_methods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
		cached_methods = ["GET", "HEAD"]
		target_origin_id = "${local.static_dist_origin_id}"

		forwarded_values {
			query_string = true

			cookies {
				forward = "all"
			}
		}

		viewer_protocol_policy = "redirect-to-https"
		min_ttl                = 0
		default_ttl            = 3600
		max_ttl                = 86400
	}
	ordered_cache_behavior {
		path_pattern = "api/*"
		allowed_methods = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
		cached_methods = ["GET", "HEAD"]
		target_origin_id = "${local.api_dist_origin_id}"
		forwarded_values {
			headers = ["Authorization"]

			query_string = true

			cookies {
				forward = "all"
			}
		}
		compress = true
		viewer_protocol_policy = "redirect-to-https"
		min_ttl = 0
		default_ttl = 0
		max_ttl = 0
	}
	viewer_certificate {
		acm_certificate_arn = "${aws_acm_certificate.certs[count.index].arn}"
		minimum_protocol_version = "TLSv1.1_2016"
		ssl_support_method = "sni-only"
	}
	restrictions {
		geo_restriction {
			restriction_type = "none"
		}
	}
}
