resource "aws_dynamodb_table" "market" {
	name = "econsim-market"
	billing_mode = "PROVISIONED"
	read_capacity = 3
	write_capacity = 3
	hash_key = "id"

	attribute {
		name = "id"
		type = "S"
	}
}

resource "aws_dynamodb_table" "team" {
	name = "econsim-team"
	billing_mode = "PROVISIONED"
	read_capacity = 3
	write_capacity = 3
	hash_key = "marketId"
	range_key = "id"

	attribute {
		name = "marketId"
		type = "S"
	}

	attribute {
		name = "id"
		type = "S"
	}
}

resource "aws_dynamodb_table" "user" {
	name = "econsim-user"
	billing_mode = "PROVISIONED"
	read_capacity = 3
	write_capacity = 3
	hash_key = "marketId"
	range_key = "id"

	attribute {
		name = "marketId"
		type = "S"
	}

	attribute {
		name = "id"
		type = "S"
	}
}

resource "aws_dynamodb_table" "resource" {
	name = "econsim-resource"
	billing_mode = "PROVISIONED"
	read_capacity = 10
	write_capacity = 10
	hash_key = "marketId"
	range_key = "id"

	attribute {
		name = "marketId"
		type = "S"
	}

	attribute {
		name = "id"
		type = "S"
	}
}
