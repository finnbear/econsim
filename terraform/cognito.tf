resource "aws_cognito_user_pool" "pool" {
	admin_create_user_config {
		allow_admin_create_user_only = false
		unused_account_validity_days = 28
	}
	lambda_config {
		pre_sign_up = "${aws_lambda_function.confirmation.arn}"
	}
	device_configuration {
		device_only_remembered_on_user_prompt = false
	}
	schema {
		name = "role"
		attribute_data_type = "String"
		required = false
		mutable = true
		string_attribute_constraints {
			max_length = 32
		}
	}
	password_policy {
		minimum_length = 8
		require_lowercase = false
		require_numbers = true
		require_symbols = false
		require_uppercase = false
	}
	name = "econsim-user"
}

data "http" "jwks" {
	url = "https://${aws_cognito_user_pool.pool.endpoint}/.well-known/jwks.json"
}

resource "aws_cognito_user_pool_client" "pool" {
	allowed_oauth_flows = ["code", "implicit"]
	allowed_oauth_flows_user_pool_client = true
	allowed_oauth_scopes = ["email", "openid", "phone", "profile"]
	callback_urls = [
		for domain in var.domains:
			"https://${domain}/"
	]
	explicit_auth_flows = ["ADMIN_NO_SRP_AUTH"]
	generate_secret = false
	logout_urls = [
		for domain in var.domains:
			"https://${domain}/"
	]
	name = "client"
	read_attributes = ["email", "email_verified", "name", "custom:role"]
	refresh_token_validity = 28
	supported_identity_providers = ["COGNITO"]
	user_pool_id = "${aws_cognito_user_pool.pool.id}"
}
