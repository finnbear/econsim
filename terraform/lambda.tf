resource "aws_iam_role" "api" {
	name = "econsim-role"
	assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [{
		"Sid": "assume",
		"Action": "sts:AssumeRole",
		"Effect": "Allow",
		"Principal": {
			"Service": "lambda.amazonaws.com"
		}
	}]
}
EOF

}

resource "aws_iam_role_policy" "api" {
	name = "econsim-policy"
	role = "${aws_iam_role.api.id}"
	policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "database",
			"Action": [
				"dynamodb:BatchGetItem",
				"dynamodb:BatchWriteItem",
				"dynamodb:PutItem",
				"dynamodb:DescribeTable",
				"dynamodb:DeleteItem",
				"dynamodb:GetItem",
				"dynamodb:Scan",
				"dynamodb:Query",
				"dynamodb:UpdateItem"
			],
			"Effect": "Allow",
			"Resource": [
				"${aws_dynamodb_table.market.arn}",
				"${aws_dynamodb_table.team.arn}",
				"${aws_dynamodb_table.user.arn}",
				"${aws_dynamodb_table.resource.arn}"
			]
		},
		{
			"Sid": "log",
			"Action": [
				"logs:CreateLogGroup",
				"logs:CreateLogStream",
				"logs:PutLogEvents"
			],
			"Effect": "Allow",
			"Resource": "arn:aws:logs:*:*:*"
		}
	]
}
EOF
}

resource "aws_lambda_function" "api" {
	function_name = "econsim-api"
	handler = "index.handler"
	runtime = "nodejs10.x"
	filename = "../api.zip"
	source_code_hash = "${filebase64sha256("../api.zip")}"
	role = "${aws_iam_role.api.arn}"
	timeout = 5
}

resource "aws_lambda_permission" "api" {
	statement_id = "econsim-api-execution"
	action = "lambda:InvokeFunction"
	function_name = "${aws_lambda_function.api.function_name}"
	principal = "apigateway.amazonaws.com"
	source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}

resource "aws_lambda_function" "confirmation" {
	function_name = "econsim-confirmation"
	handler = "index.handler"
	runtime = "nodejs10.x"
	filename = "../confirmation.zip"
	source_code_hash = "${filebase64sha256("../confirmation.zip")}"
	role = "${aws_iam_role.api.arn}"
	timeout = 5
}

resource "aws_lambda_permission" "confirmation" {
	statement_id = "econsim-confirmation-execution"
	action = "lambda:InvokeFunction"
	function_name = "${aws_lambda_function.confirmation.function_name}"
	principal = "cognito-idp.amazonaws.com"
	source_arn = "${aws_cognito_user_pool.pool.arn}"
}
