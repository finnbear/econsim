resource "aws_acm_certificate" "certs" {
	count = "${length(var.domains)}"
	domain_name = "${element(var.domains, count.index)}"
	lifecycle {
		create_before_destroy = true
	}
	subject_alternative_names = [
		"*.${element(var.domains, count.index)}"
	]
	validation_method = "DNS"
}

resource "aws_acm_certificate_validation" "certs" {
	count = "${length(var.domains)}"
	certificate_arn = "${aws_acm_certificate.certs[count.index].arn}"
	validation_record_fqdns = ["${aws_route53_record.cert[count.index].fqdn}"]
}
