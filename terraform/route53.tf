
resource "aws_route53_zone" "zones" {
	count = "${length(var.domains)}"
	name = "${element(var.domains, count.index)}"
}

resource "aws_route53_record" "zone_alias_cloudfront" {
	count = "${length(var.domains)}"
	zone_id = "${aws_route53_zone.zones[count.index].zone_id}"
	name = "${aws_route53_zone.zones[count.index].name}"
	type = "A"
	alias {
		name = "${aws_cloudfront_distribution.distributions[count.index].domain_name}"
		zone_id = "${aws_cloudfront_distribution.distributions[count.index].hosted_zone_id}"
		evaluate_target_health = false
	}
}

resource "aws_route53_record" "zone_alias_cloudfront_www" {
	count = "${length(var.domains)}"
	zone_id = "${aws_route53_zone.zones[count.index].zone_id}"
	name = "www.${aws_route53_zone.zones[count.index].name}"
	type = "A"
	alias {
		name = "${aws_cloudfront_distribution.distributions[count.index].domain_name}"
		zone_id = "${aws_cloudfront_distribution.distributions[count.index].hosted_zone_id}"
		evaluate_target_health = false
	}
}

resource "aws_route53_record" "cert" {
	count = "${length(var.domains)}"
	name = "${aws_acm_certificate.certs[count.index].domain_validation_options.0.resource_record_name}"
	type = "${aws_acm_certificate.certs[count.index].domain_validation_options.0.resource_record_type}"
	records = ["${aws_acm_certificate.certs[count.index].domain_validation_options.0.resource_record_value}"]
	ttl = 900
	zone_id = "${aws_route53_zone.zones[count.index].id}"
}
