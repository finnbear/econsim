import csv
import json

types = ["ideas", "inventionStrategies", "marketPhases", "lawsuitStrategies"]
bundle = {}

bundle["roles"] = ["entrepreneur", "inventor", "marketer", "lawyer"]

for type in types:
	bundle[type] = []
	with open(type + ".csv") as file:
		rows = csv.reader(file, delimiter=',')
		next(rows)
		enabled = False
		group = ""
		for row in rows:
			if type == "ideas":
				if len(row[0] + row[1]) > 0:
					enabled = row[0] == "TRUE"

				if len(row[1]) > 0:
					group = row[1].lower()

				if enabled and len(row[5]) > 0:
					idea = {"name": row[4], "group": group.replace(" ", "-"), "patent": group + " " + row[5].lower(), "level": int(row[2])}

					if len(row[8]) > 0:
						idea["sprite"] = int(row[8])

					bundle[type].append(idea)

			elif type == "inventionStrategies":
				bundle[type].append({"name": row[1], "id": row[1].lower().replace(" ", "-"), "cost": int(row[2]), "maxIdeas": int(row[3]), "minLevel": int(row[4]), "maxLevel": int(row[5]), "minLevelIncrement": int(row[6]), "maxLevelIncrement": int(row[7])})
			elif type == "marketPhases":
				bundle[type].append({"name": row[1], "id": row[1].lower(), "revenueShare": float(row[2])})
			elif type == "lawsuitStrategies":
				bundle[type].append({"name": row[2], "id": row[1], "header": row[3], "decision": row[4]})

print(json.dumps(bundle, indent=2))

with open('data.json', 'w') as file:
	json.dump(bundle, file)
