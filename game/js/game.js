exports.update = async function(market, type, subjectId, action, database) {
	const user = market.users.find(user => user.id == market.userId);

	switch (type) {
		case "market":
			requireRole(user, "admin");

			switch (action.type) {
				case "changeStatus":
					market.status = action.value;

					if (database) {
						await database.updateMarket({id: market.id, status: market.status});
					}
					break;
				case "createTeams":
					const players = market.users.filter(user => !user.roles.includes("admin"));
					const teamCountRequired = Math.ceil(players.length / 4);
					while (market.teams.length < teamCountRequired) {
						 await createTeam(market, database);
					}
					const roleOrder = ["marketer", "lawyer", "inventor", "entrepreneur"];
					for (let i = 0; i < players.length; i++) {
						const player = players[i];
						player.teamId = market.teams[Math.floor(i / 4)].id;

						player.roles = [roleOrder[i % 4]];

						if (i == players.length - 1) {
							let j = i;
							while (++j % 4 > 0) {
								player.roles.push(roleOrder[j % 4]);
							}
						}

						if (database) {
							await database.updateUser({id: player.id, marketId: player.marketId, teamId: player.teamId, roles: player.roles});
						}
					}
					return {teams: market.teams, users: market.users};
			}
			break;
		case "team":
			const team = market.teams.find(team => team.id == subjectId);
			const updatedTeam = team && database ? {id: team.id, marketId: team.marketId} : team;

			switch (action.type) {
				case "create":
					requireRole(user, "admin");
					return await createTeam(market, database);
				case "budget":
					requireTeam(user, team.id);
					requireRole(user, "entrepreneur");
					if (action.value > team.balance) {
						throw new GameError("Insufficient balance.", 403);
					}

					if (database) {
						await database.updateTeam({...updatedTeam, balance: () => -action.value, [action.role + "Budget"]: () => action.value});
					}

					updatedTeam.balance = team.balance - action.value;
					updatedTeam[action.role + "Budget"] = team[action.role + "Budget"] + action.value;
					break;
				case "hire":
					requireTeam(user, team.id);
					requireRole(user, "lawyer");
					if (action.level > team.lawyerBudget) {
						throw new GameError("Insufficient budget.", 403);
					}
					if (database) {
						await database.updateTeam({...updatedTeam, lawyerBudget: () => -action.level});
					}

					updatedTeam.lawyerBudget = team.lawyerBudget - action.level;

					const hiredLawyer = {marketId: market.id, teamId: team.id, type: "lawyer", level: action.level};
					if (database) {
						await database.createResource(hiredLawyer);
					} else {
						market.resources.push(hiredLawyer);
					}
					break;
				case "changeInventionStrategy":
					requireTeam(user, team.id);
					requireRole(user, "inventor");
					updatedTeam.inventionStrategy = action.value;
					if (database) {
						await database.updateTeam(updatedTeam);
					}
					break;
				case "changeLawsuitStrategy":
					requireTeam(user, team.id);
					requireRole(user, "lawyer");
					updatedTeam.lawsuitStrategy = action.value;
					if (database) {
						await database.updateTeam(updatedTeam);
					}
					break;
				case "delete":
					requireRole(user, "admin");
					if (database) {
						await database.deleteTeam(team);
					}
					return {};
			}

			return updatedTeam;
		case "user":
			const userToUpdate = market.users.find(user => user.id == subjectId);
			const updatedUser = userToUpdate && database ? {id: userToUpdate.id, marketId: userToUpdate.marketId} : userToUpdate;

			switch (action.type) {
				case "changeTeam":
					requireRole(user, "admin");
					updatedUser.teamId = action.value;
					break;
				case "addRole":
					requireRole(user, "admin");
					if (userToUpdate.roles.includes(action.value)) {
						throw new GameError("Cannot add existing role");
					} else {
						updatedUser.roles = [...userToUpdate.roles, action.value];
					}
					break;
				case "changeRoles":
					requireRole(user, "admin");
					if (!Array.isArray(action.value)) {
						throw new GameError("Invalid role " + action.value, 500);
					}
					updatedUser.roles = action.value;
					break;
				case "removeRole":
					requireRole(user, "admin");
					if (userToUpdate.roles.includes(action.value)) {
						updatedUser.roles = userToUpdate.roles.filter(role => role != action.value);
					} else {
						throw new GameError("Cannot remove nonexistent role");
					}
					break;
			}

			if (database) {
				await database.updateUser(updatedUser);
			}
			return updatedUser;
		case "resource":
			const resource = market.resources.find(resource => resource.id == subjectId);
			const updatedResource = database ? {id: resource.id, marketId: resource.marketId} : resource;

			switch (action.type) {
				case "move":
					if (action.status === "trialing") {
						requireRole(user, "admin");
					} else {
						requireTeam(user, resource.teamId);
					}

					if (action.status) {
						if (action.status === resource.status) {
							throw new GameError(`Resource was already ${resource.status}`);
						}
						updatedResource.status = action.status;
						updatedResource.input = null;
						updatedResource.output = null;
						switch (action.status) {
							case "marketing":
								updatedResource.progress = "introduction";
								break;
							case "holding":
								updatedResource.progress = "valid";
								break;
							case "trialing":
								updatedResource.progress = "jury";
								break;
							default:
								updatedResource.progress = action.status.endsWith("ing") ? 1 / ((resource.level || 3) + 1) : null;
						}
					} else {
						updatedResource.status = "transferring";
						updatedResource.input = action.input;
						updatedResource.output = action.output;
						updatedResource.progress = null;
					}
					break;
				case "addEvidence":
					if (resource.type != "product" || resource.status != "marketing") {
						throw new GameError("Only products on the market can be made into evidence.");
					}
					if (resource.teamId == user.teamId) {
						throw new GameError("A team cannot create evidence out of their own products.")
					}
					if (market.resources.find(existingResource => existingResource.teamId == user.teamId && existingResource.type == "evidence" && existingResource.id == resource.id)) {
						throw new GameError("Evidence already exists for this resource.");
					}
					const resourceTeam = market.teams.find(team => team.id == user.teamId);

					if (resourceTeam.marketerBudget < 5) {
						throw new GameError("Insufficient budget.");
					}

					const evidence = {marketId: resource.marketId, teamId: user.teamId, resourceId: resource.id, type: "evidence", status: "transferring", output: "marketer", input: "lawyer"};
					if (database) {
						await database.updateTeam({id: resourceTeam.id, marketId: resourceTeam.marketId, marketerBudget: () => -5})
						return await database.createResource(evidence);
					} else {
						market.resources.push(evidence);
						resourceTeam.marketerBudget -= 5;
					}
					break;
				case "changeProgress":
					if (resource.type != "lawsuit") {
						throw new GameError("Only lawsuits can receive progress updates");
					}
					if (resource.status != "trialing") {
						throw new GameError("Lawsuit is not at trial.");
					}
					requireRole(user, "admin");
					if (["jury", "settled", "dismissed"].includes(action.value)) {
						updatedResource.progress = action.value;

						if (action.value === "settled" && action.decision) {
							console.log("Settling lawsuit...");
							const plaintiff = market.teams.find(team => team.id == updatedResource.teamId);
					        const infringement = market.resources.find(resource => resource.id == updatedResource.resourceId);

					        let patent;
					        if (plaintiff && infringement) {
					            // Give the plaintiff the benefit of the doubt by finding the earliest valid (TODO) patent
					            const patents = market.resources.filter(resource => resource.teamId == plaintiff.id && resource.type == "patent" && resource.group == infringement.group && resource.level == infringement.level);
					            patents.sort((a, b) => a.created - b.created);
					            patent = patents[0];
					        }

					        let defendant;
					        if (infringement) {
					            defendant = market.teams.find(team => team.id == infringement.teamId);
					        }

							console.log(`Decision was to ${action.decision}`);

							let multiplier = action.multiplier || 1;

							switch(action.decision) {
								case "stop":
									if (infringement) {
										if (infringement.status === "marketing") {
											console.log("Banning infringement");
											infringement.progress = "banned";
											if (database) {
												await database.updateResource({id: infringement.id, marketId: updatedResource.marketId, progress: infringement.progress});
											}
										} else {
											console.log("infringement was not marketing");
											console.log(infringement);
										}
									} else {
										console.log("No infringement found.");
									}
									break;
								case "compensate":
									if (infringement) {
										console.log("Transferring resource to plaintiff");
										infringement.teamId = plaintiff.teamId;
										if (database) {
											await database.updateResource({id: infringement.id, marketId: updatedResource.marketId, teamId: infringement.teamId});
										}
									} else {
										console.log("No infringement found");
									}
									break;
								case "pay":
									multiplier *= 5;
									break;
							}

							const payment = 100 * multiplier;

							if (plaintiff) {
								console.log("Compensating plaintiff");
								if (database) {
									await database.updateTeam({id: plaintiff.id, marketId: updatedResource.marketId, balance: () => payment, lawyerWinnings: () => payment});
								} else {
									plaintiff.balance += payment;
									plaintiff.lawyerWinnings += payment;
								}
							} else {
								console.log("No plaintiff found");
							}
							if (defendant) {
								console.log("Penalizing defendant");
								if (database) {
									await database.updateTeam({id: defendant.id, marketId: updatedResource.marketId, balance: () => -payment, lawyerWinnings: () => -payment});
								} else {
									plaintiff.balance -= payment;
									plaintiff.lawyerWinnings -= payment;
								}
							} else {
								console.log("No defendant found");
							}
						}
					} else {
						throw new GameError("Invalid lawsuit progress.");
					}
					break;
			}

			if (database) {
				await database.updateResource(updatedResource);
			}

			return updatedResource;
	}
}

const GameError = function GameError(message, code) {
	this.message = message;
	this.code = code;

	this.toString = function() {
		return `${this.message} (${this.code || 500})`;
	}
}

exports.GameError = GameError;

const createTeam = async function(market, database) {
	let newTeam = {marketId: market.id, name: "Team " + (market.teams.length + 1), balance: 10, inventorBudget: 0, marketerBudget: 0, marketerRevenue: 0, lawyerBudget: 0, lawyerWinnings: 0};

	if (database) {
		newTeam = await database.createTeam(newTeam);
	}

	market.teams.push(newTeam);

	return newTeam;
}

const requireTeam = function(user, teamId) {
	if (user.teamId != teamId) {
		throw new GameError("Required team " + teamId + ", found " + user.teamId, 403);
	}
}

const requireRole = function(user, role) {
	if (!user.roles.includes(role)) {
		throw new GameError("Required role " + role + ", found " + user.roles, 403);
	}
}
