'use strict';

const database = require("./database.js");
const data = require("./data.json");
const game = require("./game.js");

exports.handler = async (event, context, callback) => {
	try {
		console.log(JSON.stringify(event))

		let path = event.path.slice(5).split("/")
		console.log("Path: [" + path.join(", ") + "]");
		path.push(null);

		const auth = event.requestContext.authorizer.claims;
		const admin = auth["custom:role"] == "admin";
		const now = new Date().valueOf();

		let response = null;

		let market = null;

		try {
			market = await database.readMarket({id: path[1]});
		} catch (err) {
			if (admin) {
				market = await database.createMarket({id: path[1], status: "open"});
			} else {
				throw err;
			}
		}

		console.log("Loading market data...");
		market.teams = await database.readTeamByMarketId(market.id);
		market.users = await database.readUserByMarketId(market.id);
		market.resources = (await database.readResourceByMarketId(market.id)).filter(resource => handleCheckCompatibility(data, resource));
		console.log("Finished loading market data.");

		let user = market.users.find(user => user.id == auth.sub);
		let team = user && user.teamId ? market.teams.find(team => team.id == user.teamId) : null;

		market.teamId = team ? team.id : null;
		market.userId = user ? user.id : null;

		switch (event.httpMethod) {
			case "GET":
				switch (path[0]) {
					case "market":
						if (!user) {
							if (market.status == "open") {
								user = await database.createUser({id: auth.sub, marketId: market.id, /*teamId: team.id,*/ roles: admin ? ["admin"] : [], name: auth["cognito:username"]});
								market.users.push(user);
							}
						}

						if (market.status == "started" && (!market.updated || (market.updated + 1000 * (10 + 5 * Math.random()) < now))) {
							console.log("Updating market...")

							for (let teamToUpdate of market.teams) {
								console.log("Updating team \"" + teamToUpdate.name + "\"...")
								const inventionStrategy = data.inventionStrategies.find(inventionStrategy => inventionStrategy.id == (teamToUpdate.inventionStrategy)) || data.inventionStrategies[0];

								if (!market.resources.find(resource => resource.teamId == teamToUpdate.id && ["idea", "invention", "product"].includes(resource.type))) {
									const starterIdea = await database.createResource({marketId: market.id, teamId: teamToUpdate.id, type: "idea", level: 1, group: data.ideas.find(idea => idea.level == 1).group, status: "transferring", input: "inventor"});
									market.resources.push(starterIdea);
								}

								if (teamToUpdate.inventorBudget >= inventionStrategy.cost && market.resources.filter(resource => resource.teamId == teamToUpdate.id && resource.type == "idea" && resource.input == "inventor").length < inventionStrategy.maxIdeas) {
									let minLevel = Math.max(1, inventionStrategy.minLevel);
									let maxLevel = Math.min(5, inventionStrategy.maxLevel);
									const groups = data.ideas.filter(idea => idea.level >= minLevel && idea.level <= maxLevel).map(idea => idea.group);
									const group = groups[Math.floor(Math.random() * groups.length)];

									const competition = market.resources.filter(resource => resource.status == "marketing" && resource.group == group);

									let minCompetitionLevel = 5;
									let maxCompetitionLevel = 1;

									for (let resource of competition) {
										minCompetitionLevel = Math.min(resource.level, minCompetitionLevel);
										maxCompetitionLevel = Math.max(resource.level, maxCompetitionLevel);
									}

									if (competition.length > 0) {
										minLevel = Math.max(minLevel, minCompetitionLevel + inventionStrategy.minLevelIncrement);
									}
									maxLevel = Math.min(maxLevel, maxCompetitionLevel + inventionStrategy.maxLevelIncrement);

									const ideas = data.ideas.filter(idea => idea.group == group && idea.level >= minLevel && idea.level <= maxLevel);

									if (ideas.length > 0) {
										const idea = ideas[Math.floor(Math.random() * ideas.length)];

										if (idea && !market.resources.find(resource => resource.type == "idea" && resource.teamId == teamToUpdate.id && resource.group == idea.group && resource.level == idea.level)) {
											teamToUpdate.inventorBudget -= inventionStrategy.cost;
											await database.updateTeam({id: teamToUpdate.id, marketId: market.id, inventorBudget: () => -inventionStrategy.cost});
											await database.createResource({marketId: market.id, teamId: teamToUpdate.id, type: "idea", level: idea.level, group: idea.group, status: "transferring", input: "inventor"});
										}
									}
								}

								// Socialism
								if (teamToUpdate.balance < 5 && (teamToUpdate.inventorBudget < 5 || teamToUpdate.marketerBudget < 5)) {
									database.updateTeam({id: teamToUpdate.id, marketId: market.id, balance: () => 1});
								}

								const progressingResources = market.resources.filter(resource => resource.teamId == teamToUpdate.id && resource.progress > 0 && resource.progress < 1);

								console.log("Found " + progressingResources.length + " progressing resources.");

								if (progressingResources.length > 0) {
									const progressingResource = progressingResources[Math.floor(Math.random() * progressingResources.length)];

									console.log("Selected " + JSON.stringify(progressingResource) + " for progression.");

									const progressingRole = {
										"inventing": "inventor",
										"producing": "marketer",
										"patenting": "lawyer",
										"examining": "lawyer"
									}[progressingResource.status];

									console.log("Budget is $" + teamToUpdate[progressingRole + "Budget"])

									let valid = true;

									if (progressingResource.type == "evidence") {
										const referencedResource = market.resources.find(resource => resource.id == progressingResource.resourceId);
										if (!referencedResource) {
											valid = false;
										} else {
											valid = market.resources.find(resource => resource.teamId == progressingResource.teamId && resource.type == "patent" && resource.status == "examining" && resource.progress >= progressingResource.progress && resource.group == referencedResource.group && resource.level == referencedResource.level) ? true : false;
										}
									}

									if (valid && teamToUpdate[progressingRole + "Budget"] >= 1) {
										teamToUpdate[progressingRole + "Budget"] -= 1;

										await database.updateTeam({id: teamToUpdate.id, marketId: market.id, [progressingRole + "Budget"]: teamToUpdate[progressingRole + "Budget"]})

										const progressedResource = {id: progressingResource.id, marketId: market.id, progress: progressingResource.progress};

										progressedResource.progress += 1 / ((progressingResource.level || 3) + 1);

										if (progressedResource.progress > 0.95) {
											progressedResource.progress = 1;
											if (progressingResource.type == "idea" && progressingResource.status == "inventing") {
												progressedResource.type = "invention";
											} else if (progressingResource.type == "invention") {
											 	if (progressingResource.status == "producing") {
													progressedResource.type = "product";
													progressedResource.produced = now;
												} else if (progressingResource.status == "patenting") {
													progressedResource.type = "patent";
													progressedResource.issued = now;
												}
											} else if (progressingResource.type == "evidence") {
												progressedResource.type = "lawsuit";
												progressedResource.filed = now;
											}
										}

										await database.updateResource(progressedResource);
									}
								}

								const marketingResources = market.resources.filter(resource => resource.teamId == teamToUpdate.id && resource.status == "marketing" && resource.progress);

								console.log("Found " + marketingResources.length + " marketing resources.");

								let marketerRevenue = 0;

								for (let marketingResource of marketingResources) {
									const phase = data.marketPhases.find(phase => phase.id == marketingResource.progress);

									if (!phase) {
										console.log(`Skipping marketing of ${marketingResource.id} which was ${marketingResource.progress}.`);
										continue;
									}

									console.log("Selected " + JSON.stringify(marketingResource) + " for marketing.");

									const marketedResource = {id: marketingResource.id, marketId: marketingResource.marketId, progress: marketingResource.progress};

									const age = marketingResource.produced ? (now - marketingResource.produced) / 60000 : 0;
									let crowding = 0;
									let obsolescence = 0;

									marketerRevenue += phase.revenueShare * marketingResource.level / (1 + crowding + obsolescence);

									const phases = ["introduction", "growth", "maturity", "decline"];

									for (let resource of market.resources.filter(resource => resource.type == "product" && resource.group == marketingResource.group && phases.indexOf(resource.progress) >= phases.indexOf(marketingResource.progress))) {
										if (resource.level >= marketingResource.level) {
											crowding += 1;
										}

										if (resource.level > marketingResource.level) {
											obsolescence += resource.level - marketingResource.level;
										}
									}

									switch (marketedResource.progress) {
										case "introduction":
											if (age + crowding > 4 || obsolescence > 0) {
												marketedResource.progress = "growth";
												await database.updateResource(marketedResource);
											}
											break;
										case "growth":
											if (age + crowding > 8 || obsolescence > 0) {
												marketedResource.progress = "maturity";
												await database.updateResource(marketedResource);
											}
											break;
										case "maturity":
											if (obsolescence > 0) {
												marketedResource.progress = "decline";
												await database.updateResource(marketedResource);
											}
											break;
										case "decline":
											if (obsolescence > 0) {
												marketedResource.progress = null;
												marketedResource.status = "dead";
												await database.updateResource(marketedResource);
											}
											break;
									}
								}

								marketerRevenue = Math.ceil(marketerRevenue);

								database.updateTeam({id: teamToUpdate.id, marketId: market.id, balance: () => marketerRevenue, marketerRevenue: () => marketerRevenue});
							}

							const marketUpdate = {
								id: market.id,
								updated: now
							}

							await database.updateMarket(marketUpdate);
						}

						response = market
						break;
				}
				break;
			case "POST":
				const putRequest = JSON.parse(event.body);
				switch (path[0]) {
					case "market":
						response = await game.update(market, path[2] || "market", path[3], putRequest, database);
						break;
				}
				break;
		}

		if (response === null) {
			throw new this.HttpError("Not found.", 404);
		}

		const responseString = JSON.stringify(response, null, 2);

		console.log("Responding with: " + responseString);

		return {
			body: responseString,
			headers: {
				"Content-Type": "application/json"
			},
			isBase64Encoded: true,
			statusCode: 200
		};
	} catch (err) {
		console.log(err);
		return {
			body: JSON.stringify({message: err ? err.toString() : "Internal server error", stack: err.stack}),
			headers: {
				"Content-Type": "application/json"
			},
			isBase64Encoded: true,
			statusCode: err && err.code ? err.code : 500
		};
	}

	console.log("WARNING: Reached end of handler without returning.")
};

exports.HttpError = function HttpError(message, code) {
	this.message = message;
	this.code = code;

	this.toString = function() {
		return `${this.message} (${this.code})`;
	}
}

const requireRole = function(user, role) {
	if (!user.roles.includes(role)) {
		throw new game.GameError("Required role " + role + ", found " + user.roles, 403);
	}
}

const handleCheckCompatibility = function(data, resource) {
	if (resource.group) {
		return data.ideas.find(idea => idea.group == resource.group && idea.level == resource.level) ? true : false;
	}

	return true;
}
