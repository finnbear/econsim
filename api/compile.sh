zip -r ../api.zip * -x ".*" -x "*.sh" -x "*.py" -x "*.csv"
aws lambda update-function-code --function-name econsim-api --zip-file fileb://../api.zip --profile terraform --region us-east-1
