const AWS = require("aws-sdk");
const Hashids = require('hashids/cjs')

const hashids = new Hashids("squirrel", 5);

exports.createMarket = async function(market) {
	return await _create(MARKET_TABLE, market);
};

exports.createTeam = async function(team) {
	return await _create(TEAM_TABLE, team);
}

exports.createUser = async function(user) {
	return await _create(USER_TABLE, user);
};

exports.createResource = async function(resource) {
	return await _create(RESOURCE_TABLE, resource);
};

exports.deleteTeam = async function(key) {
	await _delete(TEAM_TABLE, key);
}

exports.deleteUser = async function(key) {
	await _delete(USER_TABLE, key);
}

exports.deleteResource = async function(key) {
	await _delete(RESOURCE_TABLE, key);
};

exports.readMarket = async function(key) {
	return await _read(MARKET_TABLE, key);
};

exports.readTeam = async function(key) {
	return await _read(TEAM_TABLE, key);
}

exports.readUser = async function(key) {
	return await _read(USER_TABLE, key);
};

exports.readResource = async function(key) {
	return await _read(RESOURCE_TABLE, key);
};

exports.readTeamByMarketId = async function(marketId) {
	return await _readBy(TEAM_TABLE, {marketId});
};

exports.readUserByMarketId = async function(marketId) {
	return await _readBy(USER_TABLE, {marketId});
};

exports.readResourceByMarketId = async function(marketId) {
	return await _readBy(RESOURCE_TABLE, {marketId});
};

exports.updateMarket = async function(market) {
	return await _update(MARKET_TABLE, market);
};

exports.updateTeam = async function(team) {
	return await _update(TEAM_TABLE, team);
}

exports.updateUser = async function(user) {
	return await _update(USER_TABLE, user);
};

exports.updateResource = async function(resource) {
	return await _update(RESOURCE_TABLE, resource);
};

const _DYNAMODB = new AWS.DynamoDB.DocumentClient();

function copyValidProperties(table, fromObj) {
	if (!(fromObj && table && table.columns)) {
		throw new Error("params");
	}
	const {columns, composite} = table;
	const {id, marketId} = fromObj;
	const toObj = composite ? {id, marketId} : {id};
	for (let k in fromObj) {
		if (k in columns) {
			const v = fromObj[k];
			const filter = columns[k];
			toObj[k] = filter ? filter(v) : v;
		}
	}

	return toObj;
}

async function _create(table, itemData) {
	const item = copyValidProperties(table, itemData);

	// For compatibility with Dynamo, use Unix format for creation timestamp.
	item.created = new Date().valueOf();

	const key = {id: item.id};

	if (table.composite) {
		key.marketId = item.marketId;
	}

	if (item.id) {
		let exists = false;
		try {
			await _read(table, key);
			exists = true;
		} catch (err) {}

		if (exists) {
			throw new DatabaseError(`Cannot create duplicate ${table.objectName}`, 500);
		}
	} else {
		for (let governor = 0; governor < 5 && !item.id; ++governor) {
			if (!item.id) {
				item.id = hashids.encode(new Date().valueOf() % 10000);
			}

			key.id = item.id;

			try {
				await _read(table, key);

				// Duplicate not allowed.
				item.id = null;
			} catch (err) {}
		}

		if (!item.id) {
			throw new DatabaseError("Cannot generate unique key", 500);
		}
	}

	await _DYNAMODB.put({
		TableName: table.name,
		Item: item
	}).promise();

	return item;
}

async function _read(table, key) {
	if (!(key && key.id)) {
		throw new DatabaseError(`id required to read ${table.objectName}`);
	}
	if (table.composite && !key.marketId) {
		throw new DatabaseError(`marketId is required to read ${table.objectName}`);
	}
	const data = await _DYNAMODB.get({
		TableName: table.name,
		Key: key,
		ConsistentRead: true
	}).promise();

	if (!data.Item) {
		throw new DatabaseError(`could not find ${table.objectName}`, 404);
	}

	return data.Item;
}

async function __query(params) {
	const items = [];

	do {
		let data = await _DYNAMODB.query(params).promise();
		items.push(...data.Items);
		params.ExclusiveStartKey = data.LastEvaluatedKey;
	} while (params.ExclusiveStartKey == true);

	return items;
}

async function _readBy(table, key) {
	let keyNames = Object.keys(key);

	let filterExpression = "";

	let expressionAttributeNames = {};
	let expressionAttributeValues = {};

	let conjunction = " AND ";

	for (let keyName of keyNames) {
		expressionAttributeNames[`#${keyName}`] = keyName;
		expressionAttributeValues[`:${keyName}`] = key[keyName];

		if (table.composite && keyName == "marketId") {
			continue;
		}

		filterExpression += ":" + keyName + "=#" + keyName + conjunction;
	}

	filterExpression = filterExpression.slice(0, -conjunction.length);

	if ("marketId" in key && table.composite) {
		const params = {
			TableName: table.name,
			KeyConditionExpression: `#marketId = :marketId`,
			ExpressionAttributeNames: expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues
		};

		if (filterExpression.length > 0) {
			console.log(filterExpression);
			params.FilterExpression = filterExpression;
		}

		return await __query(params);
	} else if (keyNames.length == 1 && table.indices.includes(keyNames[0])) {
		const params = {
			TableName: table.name,
			IndexName: keyNames[0],
			KeyConditionExpression: `#${keyNames[0]} = :${keyNames[0]}`,
			ExpressionAttributeNames: expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues
		};

		return await __query(params);
	} else {
		throw new DatabaseError("scan attempted");
	}
}

async function _delete(table, item) {
	if (!(item && item.id)) {
		throw new DatabaseError(`id required to delete ${table.objectName}`);
	}

	const key = {id: item.id};

	if (table.composite) {
		if (!item.marketId) {
			throw new DatabaseError(`marketId is required to delete ${table.objectName}`);
		}

		key.marketId = item.marketId;
	}

	await _DYNAMODB.delete({
		TableName: table.name,
		Key: key
	}).promise();
}

async function _update(table, itemData) {
	const item = copyValidProperties(table, itemData);

	const key = {
		"id": item.id
	};

	if (table.composite) {
		key.marketId = item.marketId;
	}

	let expressionAttributeNames = {"#id": "id"};
	let expressionAttributeValues = {":id": item.id};
	if (table.composite) {
		expressionAttributeNames["#marketId"] = "marketId";
		expressionAttributeValues[":marketId"] = item.marketId;
	}

	let update = false;
	let add = false;
	let remove = false;

	let updateExpression = "set ";
	let addExpression = " add ";
	let removeExpression = " remove ";

	for (let k in item) {
		if (k == "id") {
			continue;
		}

		// Never update market if it is part of the key
		if (k == "marketId" && table.composite) {
			continue;
		}

		let value = item[k];

		if (value == null) {
			removeExpression += `#${k}, `;
			remove = true;
		} else if (typeof value == "function") {
			addExpression += `#${k} :${k}, `;
			expressionAttributeValues[`:${k}`] = value();
			add = true;
		} else {
			updateExpression += `#${k} = :${k}, `;
			expressionAttributeValues[`:${k}`] = value;
			update = true;
		}

		expressionAttributeNames[`#${k}`] = k;
	}

	const expression = (update ? updateExpression.substring(0, updateExpression.length - 2) : "") + (add ? addExpression.substring(0, addExpression.length - 2) : "") + (remove ? removeExpression.substring(0, removeExpression.length - 2) : "");

	console.log(item);
	console.log(expression);

	const params = {
		TableName: table.name,
		Key: key,
		UpdateExpression: expression,
		ConditionExpression: table.composite ? "#marketId = :marketId AND #id = :id" : "#id = :id",
		ExpressionAttributeNames: expressionAttributeNames
	};

	if (Object.keys(expressionAttributeValues).length > 0) {
		params.ExpressionAttributeValues = expressionAttributeValues;
	}

	await _DYNAMODB.update(params).promise();

	return item;
}

function DatabaseError(message, code) {
	this.message = message;
	this.code = code;

	this.toString = function() {
		return `${this.message} (${this.code})`;
	}
}

exports.DatabaseError = DatabaseError;

const MARKET_TABLE = {
	composite: false,
	columns: {name: false, owner: false, status: false, updated: false, type: false},
	name: `econsim-market`,
	objectName: "market",
	indices: []
};

const TEAM_TABLE = {
	composite: true,
	columns: {balance: false, inventorBudget: false, inventionStrategy: false, marketerBudget: false, marketerRevenue: false, lawsuitStrategy: false, lawyerBudget: false, lawyerWinnings: false, name: false},
	name: `econsim-team`,
	objectName: "team",
	indices: []
};

const USER_TABLE = {
	composite: true,
	columns: {roles: false, teamId: false, name: false},
	name: `econsim-user`,
	objectName: "user",
	indices: []
};

const RESOURCE_TABLE = {
	composite: true,
	columns: {name: false, group: false, input: false, issued: false, level: false, output: false, owner: false, produced: false, progress: false, resourceId: false, status: false, teamId: false, type: false, value: false},
	name: `econsim-resource`,
	objectName: "resource",
	indices: []
};
