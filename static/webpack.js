const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    mode: "production",
    context: __dirname,
    devtool: "source-map",
    entry: {
        client: __dirname + "/src/js/Index.js",
    },
    module: {
        rules: [
            {
                loader: "babel-loader",
                include: __dirname + "/src",
                query: {
                    compact: true,
                    presets: ["react"], // Don't need "env" / "es2015"
                    plugins: ["transform-object-rest-spread"]
                },
                test: /\.(js|jsx)$/i
            },
            {
                loader: "url-loader",
                test: /\.(jpe?g|png|gif|svg)$/i
            }
        ]
    },
    node: {
        fs: "empty"
    },
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                sourceMap: true,
                test: /\.js(\?.*)?$/i,
                parallel: true,
                cache: true
            })
        ]
    },
    output: {
        path: __dirname + "/build",
        pathinfo: false,
        filename: "[name].min.js"
    },
    plugins: [],
    resolve: {
        modules: [__dirname + "/src", __dirname + "/src/js", __dirname + "/src/stubs", "node_modules"],
    }
};
