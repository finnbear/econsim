./node_modules/.bin/webpack --config webpack.js
aws s3 cp ./build/client.min.js s3://econsim-static/client.min.js --profile finnbear --cache-control "no-cache"
aws s3 cp ./build/client.min.js.map s3://econsim-static/client.min.js.map --profile finnbear --cache-control "no-cache"
aws s3 cp ./src/html/index.html s3://econsim-static/index.html --profile finnbear
