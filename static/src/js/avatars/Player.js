import {Component, Fragment} from "react";
import EntrepreneurAvatar from "../../images/avatars/player/Entrepreneur.png";
import InventorAvatar from "../../images/avatars/player/Inventor.png";
import MarketerAvatar from "../../images/avatars/player/Marketer.png";
import LawyerAvatar from "../../images/avatars/player/Lawyer.png";
import Button from "../components/Button";
import {capitalize} from "../util/Util.js"

import {HEADER_STYLE} from "../style/CSS";
import {BLACK, DARK_GRAY} from "../style/Colors";

export default class Player extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onBudget: handleBudget.bind(this)
		}
	}

	render() {
		const {handlers, props} = this;
		const {onBudget} = handlers;
		const {balance, role} = props;

		return (
			<td>
				<img src={_AVATARS[role]} style={_PLAYER_STYLE}/>
				<h4 style={HEADER_STYLE}>{capitalize(role)}</h4>
				{balance !== undefined ? <Fragment>
					{[1, 5].map(value => <Button disabled={balance < value} key={value.toString()} onClick={onBudget(value)} text={`$${value}`}/>)}
				</Fragment> : null}
			</td>
		);
	}
}

const handleBudget = function(value) {
	const {props} = this;
	const {onActionOnTeam, role} = props;

	return function() {
		onActionOnTeam(null, {type: "budget", role, value});
	}
}

const _AVATARS = {
	"entrepreneur": EntrepreneurAvatar,
	"inventor": InventorAvatar,
	"marketer": MarketerAvatar,
	"lawyer": LawyerAvatar
};

const _PLAYER_STYLE = {
	height: 75,
	margin: "auto",
	width: 75
};
