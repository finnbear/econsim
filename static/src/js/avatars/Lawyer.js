import {Component, Fragment} from "react";
import {capitalize} from "../util/Util.js"
import InternAvatar from "../../images/avatars/lawyer/Intern.png";
import AssociateAvatar from "../../images/avatars/lawyer/Associate.png";
import PartnerAvatar from "../../images/avatars/lawyer/Partner.png";
import Button from "../components/Button";
import {HEADER_STYLE} from "../style/CSS";
import {BLACK, DARK_GRAY} from "../style/Colors";

export default class Lawyer extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onHire: handleHire.bind(this)
		}
	}

	render() {
		const {handlers, props} = this;
		const {onHire} = handlers;
		const {budget, level} = props;

		let value = 0;
		let name = ""

		switch (level) {
			case 1:
				value = 1;
				name = "Intern"
				break;
			case 2:
				value = 2;
				name = "Associate"
				break;
			case 3:
				value = 5;
				name = "Partner"
				break;
		}

		return (
			<td>
				<img src={_AVATARS[level - 1]} style={_LAWYER_STYLE}/>
				<h4 style={HEADER_STYLE}>{name}</h4>
				{budget !== undefined ? <Fragment>
					<Button disabled={budget < value} onClick={onHire} text={`Hire ($${value})`}/>
				</Fragment> : null}
			</td>
		);
	}
}

const handleHire = function(value) {
	const {props} = this;
	const {onActionOnTeam, level} = props;

	onActionOnTeam(null, {type: "hire", level});
}

const _AVATARS = [InternAvatar, AssociateAvatar, PartnerAvatar];

const _LAWYER_STYLE = {
	height: 75,
	margin: "auto",
	width: 75
};
