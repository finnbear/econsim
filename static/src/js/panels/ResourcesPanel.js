import {Component} from "react";
import ResourceCard from "../cards/ResourceCard";
import {FLEX_BOX_STYLE, HEADER_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"

export default class ResourcesPanel extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onDragEnter: handleDragEnter.bind(this),
			onDragLeave: handleDragLeave.bind(this),
			onDragOver: handleDragOver.bind(this),
			onDrop: handleDrop.bind(this),
			onFilter: handleFilter.bind(this)
		}

		this.state = {dragOver: false};
	}

	render() {
		const {handlers, props, state} = this;
		const {onDragEnter, onDragOver, onDragLeave, onDrop} = handlers;
		const {name, children, colSpan, data, filter, market, onMessage, resources, rowSpan} = props;
		const {dragOver} = state;

		const tableDataStyle = {
			...TABLE_DATA_DRAG_STYLE,
			...(dragOver ? {backgroundColor: "#55efc4", borderColor: "#00b894"} : {})
		}

		const scale = colSpan > 1 ? 2 : resources.length <= 2 ? 1 : 0.5;

		const cardProps = {
			data,
			market,
			scale
		};

		let message = null;

		if (onMessage) {
			message = onMessage(resources);
		} else if (!children && resources.length == 0) {
			if (filter === false) {
				message = "Cards will appear here";
			} else {
				if (typeof filter == "string") {
					message = `Drag ${filter} cards here`;
				} else {
					message = "Drag cards here";
				}
			}
		}

		return (
			<td onDragEnter={onDragEnter} onDragOver={onDragOver} onDragLeave={onDragLeave} onDrop={onDrop} colSpan={colSpan} rowSpan={rowSpan} style={tableDataStyle}>
				<h2 style={HEADER_STYLE}>{name}</h2>
				{resources.length > 0 ? <div style={FLEX_BOX_STYLE}>
					{resources.map(resource => <ResourceCard key={resource.id} resource={resource} {...cardProps}/>)}
				</div> : null}
				{message ? <p style={TRANSPARENT_STYLE}>{message}</p> : null}
				{children}
			</td>
		);
	}
}

ResourcesPanel.defaultProps = {
	colSpan: 1,
	maxCards: 2,
	rowSpan: 1
};

const handleFilter = function(event) {
	const {props} = this;
	const {filter, maxCards, resources} = props;
	let type = event.dataTransfer.types[0];
	if (type.includes("json")) {
		type = event.dataTransfer.types[1];
	}
	return (maxCards == 0 || resources.length < maxCards) && (filter === true || (typeof filter == "string" && filter == type) || (Array.isArray(filter) && filter.includes(type)) || (typeof filter == "function" && filter(type)))
}

const handleDragEnter = function(event) {
	const {handlers} = this;
	const {onFilter} = handlers;

	if (onFilter(event)) {
		this.setState({dragOver: true});
	}
}

const handleDragLeave = function(event) {
	this.setState({dragOver: false});
}

const handleDragOver = function(event) {
	const {handlers} = this;
	const {onFilter} = handlers;

	if (onFilter(event)) {
		event.preventDefault();
		this.setState({dragOver: true});
	}
}

const handleDrop = function(event) {
	const {handlers, props} = this;
	const {onDragLeave} = handlers;
	const {action, onActionOnResource} = props;
	onDragLeave(event);

	const resourceJson = event.dataTransfer.getData("application/json");
	const resource = JSON.parse(resourceJson);

	if (action) {
		onActionOnResource(resource.id, action);
	}
}
