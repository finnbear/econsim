import {Component, Fragment} from "react";
import LawsuitCard from "../cards/LawsuitCard";
import {BUTTON_STYLE, FLEX_BOX_STYLE, HEADER_STYLE, TABLE_STYLE, TABLE_DATA_STYLE, TRANSPARENT_STYLE} from "../style/CSS"

export default class CourtPanel extends Component {
	constructor(props) {
		super(...arguments);

        this.handlers = {
            onChangeFilterLawsuits: handleChangeFilterLawsuits.bind(this)
        };

        this.state = {filterLawsuits: "active"};
	}

	static getDerivedStateFromProps(props) {
		const {data, market} = props;
		const {resources} = market;

		const courtLawsuits = resources.filter(resource => resource.type == "lawsuit" && (resource.output == "lawyer" || resource.status == "trialing"));

		const groupByProgress = {};

		for (let lawsuit of courtLawsuits.sort((a, b) => a.created < b.created ? 1 : a.created == b.created ? 0 : -1)) {
			if (lawsuit.progress in groupByProgress) {
				groupByProgress[lawsuit.progress].push(lawsuit);
			} else {
				groupByProgress[lawsuit.progress] = [lawsuit];
			}
		}

		return {courtLawsuits, groupByProgress};
	}

	render() {
		const {handlers, props, state} = this;
        const {onChangeFilterLawsuits} = handlers;
		const {colSpan, data, market, onActionOnResource, resources, rowSpan} = props;
		const {courtLawsuits, filterLawsuits, groupByProgress, width} = state;

		const selectionId = props.selectionId || state.selectionId;

		let selection = null;

        const cardProps = {
			data,
			market,
            onActionOnResource,
			scale: 2
		};

		return (
			<td colSpan={colSpan} rowSpan={rowSpan} style={TABLE_DATA_STYLE}>
				<h2 style={HEADER_STYLE}>Court Queue</h2>
                <select onChange={onChangeFilterLawsuits} value={filterLawsuits} style={BUTTON_STYLE}>
                    {["active", "completed", "all"].map(filterOption => <option value={filterOption}>{`Show ${filterOption} lawsuits`}</option>)}
                </select>
                {courtLawsuits.length > 0 ? <div style={FLEX_BOX_STYLE}>
					{courtLawsuits.map(lawsuit => (filterLawsuits === "all") || (["dismissed", "settled"].includes(lawsuit.progress) && filterLawsuits == "completed") || (["jury", null, undefined].includes(lawsuit.progress) && filterLawsuits == "active") ? <LawsuitCard key={lawsuit.id} lawsuit={lawsuit} {...cardProps}/> : null)}
				</div> : null}
			</td>
		);
	}
}

CourtPanel.defaultProps = {
	colSpan: 1,
	rowSpan: 1
};

const handleChangeFilterLawsuits = function(event) {
	const filterLawsuits = event.target.value;
	this.setState({filterLawsuits});
}
