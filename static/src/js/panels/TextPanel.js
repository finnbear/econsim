import {Component} from "react";
import {HEADER_STYLE, TABLE_DATA_STYLE} from "../style/CSS"

export default class TextPanel extends Component {
	constructor(props) {
		super(...arguments);
	}

	render() {
		const {props} = this;
		const {name, value} = props;

		return (
			<td style={TABLE_DATA_STYLE}>
				<h2 style={HEADER_STYLE}>{name}</h2>
				<p>{value}</p>
			</td>
		);
	}
}
