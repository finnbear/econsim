import {Component} from "react";
import {HEADER_STYLE, TABLE_DATA_STYLE} from "../style/CSS";
import {REDS} from "../style/Colors";

export default class MoneyPanel extends Component {
	constructor(props) {
		super(...arguments);
	}

	render() {
		const {props} = this;
		const {name, value} = props;

		const cellStyle = {
			...TABLE_DATA_STYLE,
			transition: "background-color 2s linear"
		};

		if (name.includes("Budget") || name.includes("Balance")) {
			if (value == 0) {
				cellStyle.backgroundColor = REDS[2];
				cellStyle.borderColor = REDS[3];
			} else if (value < 5) {
				cellStyle.backgroundColor = REDS[1];
				cellStyle.borderColor = REDS[2];
			} else if (value < 10) {
				cellStyle.backgroundColor = REDS[0];
				cellStyle.borderColor = REDS[1];
			}
		}

		return (
			<td style={cellStyle}>
				<h2 style={HEADER_STYLE}>{name}</h2>
				<p style={_VALUE_STYLE}>{value < 0 ? '-' : ''}${Math.abs(value)}</p>
			</td>
		);
	}
}

const _VALUE_STYLE = {
	fontSize: 50,
	margin: 0
};
