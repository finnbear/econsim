import {Component, Fragment} from "react";
import {Image, Layer, Line, Rect, Shape, Stage, Text} from "react-konva/lib/ReactKonvaCore";
import "konva/lib/shapes/Image";
import "konva/lib/shapes/Line";
import "konva/lib/shapes/Rect";
import "konva/lib/Shape";
import "konva/lib/shapes/Text";
import ReactResizeDetector from 'react-resize-detector';
import {FLEX_BOX_STYLE, HEADER_STYLE, TABLE_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"
import {BLACK, LIGHT_GREEN} from "../style/Colors";
import {sprite} from "../util/Util.js";

export default class MarketPanel extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onDragEnter: handleDragEnter.bind(this),
			onDragLeave: handleDragLeave.bind(this),
			onDragOver: handleDragOver.bind(this),
			onDrop: handleDrop.bind(this),
			onFilter: handleFilter.bind(this),
			onResize: handleResize.bind(this),
			onSelect: handleSelect.bind(this)
		};

		this.state = {dragOver: false, width: 100};
	}

	static getDerivedStateFromProps(props) {
		const {data, market} = props;
		const {resources} = market;

		const marketPhaseIds = [];
		const groupByProgress = {};

		for (let marketPhase of data.marketPhases) {
			marketPhaseIds.push(marketPhase.id);
			groupByProgress[marketPhase.id] = {};
		}

		const marketResources = resources.filter(resource => resource.status == "marketing" && marketPhaseIds.includes(resource.progress));

		for (let resource of marketResources.sort((a, b) => a.level < b.level ? 1 : a.level == b.level ? 0 : -1)) {
			if (resource.group in groupByProgress[resource.progress]) {
				groupByProgress[resource.progress][resource.group].push(resource);
			} else {
				groupByProgress[resource.progress][resource.group] = [resource];
			}
		}

		/*
		for (let marketPhase of data.marketPhases) {
			Object.keys(groupByProgress[marketPhase.id]).sort((a, b) => (["maturity", "decline"].includes(marketPhase.id) ? 1 : -1) * groupByProgress[marketPhase.id][b].length - groupByProgress[marketPhase.id][a].length).forEach(key => { let value = groupByProgress[marketPhase.id][key]; delete groupByProgress[marketPhase.id][key]; groupByProgress[marketPhase.id][key] = value; });
		}
		*/

		return {marketResources, groupByProgress};
	}

	/*
	shouldComponentUpdate(nextProps, nextState) {
		const {props, state} = this;
		return props.market != nextProps.market || state.width != nextState.width || JSON.stringify(state.marketResources) != JSON.stringify(nextState.marketResources);
	}
	*/

	render() {
		const {handlers, props, state} = this;
		const {onDragEnter, onDragOver, onDragLeave, onDrop, onResize, onSelect} = handlers;
		const {name, colSpan, data, market, resources, rowSpan} = props;
		const {dragOver, marketResources, groupByProgress, width} = state;

		const selectionId = props.selectionId || state.selectionId;

		const tableDataStyle = {
			...TABLE_DATA_DRAG_STYLE,
			...(dragOver ? {backgroundColor: "#55efc4", borderColor: "#00b894"} : {})
		}

		let padding = 0.01 * width;
		let resourceSize = 32;

		let selection = null;

		return (
			<td onDragEnter={onDragEnter} onDragOver={onDragOver} onDragLeave={onDragLeave} onDrop={onDrop} colSpan={colSpan} rowSpan={rowSpan} style={tableDataStyle}>
				<h2 style={HEADER_STYLE}>{name}</h2>
				<Stage height={width * 0.8} width={width} onClick={onSelect}>
					<Layer>
						<Shape
							sceneFunc={(context, shape) => {
								context.beginPath();
								context.moveTo(padding, 0.8 * width - padding);
								context.lineTo(padding, 0.6 * width);
								context.quadraticCurveTo(0.15 * width, 0.6 * width, 0.25 * width, 0.5 * width);
								context.lineTo(0.25 * width, 0.8 * width - padding);
								context.closePath();
								context.fillStrokeShape(shape);
							}}
							fill={LIGHT_GREEN}
							stroke={BLACK}
							strokeWidth={3}
						/>
						<Shape
							sceneFunc={(context, shape) => {
								context.beginPath();
								context.moveTo(0.25 * width, 0.8 * width - padding);
								context.lineTo(0.25 * width, 0.5 * width);
								context.bezierCurveTo(0.3 * width, 0.45 * width, 0.4 * width, 0.175 * width, 0.5 * width, 0.1 * width);
								context.lineTo(0.5 * width, 0.8 * width - padding);
								context.closePath();
								context.fillStrokeShape(shape);
							}}
							fill={LIGHT_GREEN}
							stroke={BLACK}
							strokeWidth={3}
						/>
						<Shape
							sceneFunc={(context, shape) => {
								context.beginPath();
								context.moveTo(0.5 * width, 0.8 * width - padding);
								context.lineTo(0.5 * width, 0.1 * width);
								context.quadraticCurveTo(0.625 * width, padding, 0.75 * width, 0.1 * width);
								context.lineTo(0.75 * width, 0.8 * width - padding);
								context.closePath();
								context.fillStrokeShape(shape);
							}}
							fill={LIGHT_GREEN}
							stroke={BLACK}
							strokeWidth={3}
						/>
						<Shape
							sceneFunc={(context, shape) => {
								context.beginPath();
								context.moveTo(0.75 * width, 0.8 * width - padding);
								context.lineTo(0.75 * width, 0.1 * width);
								context.quadraticCurveTo(0.85 * width, 0.175 * width, width - padding, 0.4 * width);
								context.lineTo(width - padding, 0.8 * width - padding);
								context.closePath();
								context.fillStrokeShape(shape);
							}}
							fill={LIGHT_GREEN}
							stroke={BLACK}
							strokeWidth={3}
						/>
						{marketResources.map(resource => {
							let x = {"introduction": 0, "growth": 1, "maturity": 2, "decline": 3}[resource.progress];

							const resourceCount = groupByProgress[resource.progress][resource.group].length;
							const groups = Object.keys(groupByProgress[resource.progress]);
							const groupCount = groups.length;
							let groupIndex = groups.indexOf(resource.group);
							let rowIndex = 0;
							let rowCount = resource.progress != "introduction" ? (groupCount > 8 ? 3 : groupCount > 4 ? 2 : 1) : 1;
							rowCount = resource.progress == "maturity" ? rowCount : Math.min(rowCount, 2);
							let rowCutoffIndex = Math.ceil(groupCount / rowCount);

							const maxResourceWidth = 0.25 * width / rowCutoffIndex;
							const maxResourceHeight = resourceSize / Math.max(resourceCount - 1, 1);

							const effectiveResourceSize = Math.min(resourceSize, maxResourceWidth, maxResourceHeight);

							if (rowCount > 1) {
								rowIndex = Math.floor(groupIndex / rowCutoffIndex);
								groupIndex = groupIndex % rowCutoffIndex;
							}
							const groupSpacing = 1 / rowCutoffIndex;
							const resourceIndex = groupByProgress[resource.progress][resource.group].indexOf(resource);

							const groupOffset = 0.05 + 0.9 * groupSpacing * (groupIndex + 0.5);

							x += groupOffset;

							let y = {"introduction": -0.08, "growth": -0.45, "maturity": 0, "decline": 0.35}[resource.progress] * groupOffset + {"introduction": 0.65, "growth": 0.6, "maturity": 0.1, "decline": 0.15}[resource.progress];

							x *= width * 0.25;

							x -= effectiveResourceSize / 2;

							y *= width;

							const rowSpacing = effectiveResourceSize * 1.05;

							y += resourceIndex * rowSpacing;

							y += rowIndex * maxResourceWidth * 2.5;

							const resourceData = data.ideas.find(idea => idea.group == resource.group && idea.level == resource.level);

							if (selectionId == resource.id) {
								const selectionTeam = market.teams.find(team => team.id == resource.teamId);
								selection = {name: resourceData.name, level: resourceData.level, teamName: selectionTeam ? selectionTeam.name : null, x: x + effectiveResourceSize * 0.5, y: y + effectiveResourceSize * 0.5};
							}

							if (resourceData.sprite !== undefined) {
								return <Image
									id={resource.id}
									x={x}
									y={y}
									image={sprite(`ideas/${resourceData.sprite}`)}
									width={effectiveResourceSize}
									height={effectiveResourceSize}
									opacity={resource.teamId == market.teamId || !market.teamId ? 1 : 0.5}
									onClick={onSelect}
								/>
							} else {
								return <Rect
									id={resource.id}
									x={x}
									y={y}
									width={effectiveResourceSize}
									height={effectiveResourceSize}
									fill={resource.teamId == market.teamId || !market.teamId ? "red" : "gray"}
									shadowBlur={2}
									stroke={BLACK}
									strokeWidth={1}
									onClick={onSelect}
								/>
							}
						})}
						{selection ? <Fragment>
							<Text
								x={padding}
								y={padding}
								width={width * 0.5}
								fontSize={20}
								align={"center"}
								wrap={"word"}
								text={selection.name}
								opacity={0.8}
							/>
							<Text
								x={padding}
								y={padding + 20}
								width={width * 0.5}
								fontSize={15}
								align={"center"}
								wrap={"word"}
								text={`[${selection.teamName}] (Level ${selection.level})`}
								opacity={0.8}
							/>
							<Line
								points={[padding + width * 0.25, padding + 35, selection.x, selection.y]}
								stroke={"black"}
								opacity={0.25}
							/>
						</Fragment> : null}
					</Layer>
				</Stage>
				<table style={TABLE_STYLE}>
					<tbody>
						<tr style={TRANSPARENT_STYLE}>
							{data.marketPhases.map(marketPhase => <td>{marketPhase.name}</td>)}
						</tr>
					</tbody>
				</table>
				<ReactResizeDetector handleWidth onResize={onResize}/>
			</td>
		);
	}
}

MarketPanel.defaultProps = {
	colSpan: 1,
	name: "Market",
	rowSpan: 1
};

const handleFilter = function(event) {
	const type = event.dataTransfer.types[0];
	return type == "product";
}

const handleDragEnter = function(event) {
	const {handlers} = this;
	const {onFilter} = handlers;

	if (onFilter(event)) {
		this.setState({dragOver: true});
	}
}

const handleDragLeave = function(event) {
	this.setState({dragOver: false});
}

const handleDragOver = function(event) {
	const {handlers} = this;
	const {onFilter} = handlers;

	if (onFilter(event)) {
		event.preventDefault();
		this.setState({dragOver: true});
	}
}

const handleDrop = function(event) {
	const {handlers, props} = this;
	const {onDragLeave} = handlers;
	const {action, onActionOnResource} = props;
	onDragLeave(event);

	const resourceJson = event.dataTransfer.getData("application/json");
	const resource = JSON.parse(resourceJson);

	if (action) {
		onActionOnResource(resource.id, action);
	}
}

const handleResize = function(width) {
	this.setState({width});
}

const handleSelect = function(event) {
	const {props} = this;
	const {onSetParentState} = props;
	const selectionId = event.target.attrs.id;
	if (onSetParentState) {
		onSetParentState({selectionId});
	} else {
		this.setState({selectionId});
	}
}
