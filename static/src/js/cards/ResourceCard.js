import {Component, Fragment} from "react";
import {capitalize} from "../util/Util"
import {LIGHT_BLUE, DARK_PINK, DARK_CYAN, DARK_ORANGE, DARK_PURPLE, DARK_RED, DARK_GREEN, BLACK, WHITE} from "../style/Colors"

export default class ResourceCard extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onDelayedDragStart: handleDelayedDragStart.bind(this),
			onDragEnd: handleDragEnd.bind(this),
			onDragStart: handleDragStart.bind(this)
		}

		this.state = {};
	}

	render() {
		const {handlers, props, state} = this;
		const {onDragEnd, onDragStart} = handlers;
		const {data, resource, market, scale} = props;
		const {dragged} = state;

		let title = resource.id;
		let text = null;

		const bannerStyle = {
			..._CARD_BANNER_STYLE,
			...(resource.progress ? {borderTop: null} : {}),
			borderTopWidth: _CARD_BANNER_STYLE.borderTopWidth * scale,
			height: _CARD_BANNER_STYLE.height * scale,
			paddingBottom: _CARD_BANNER_STYLE.paddingBottom * scale,
			paddingTop: _CARD_BANNER_STYLE.paddingTop * scale
		};

		const progressBarStyle = {
			..._CARD_PROGRESS_BAR_STYLE,
			bottom: _CARD_PROGRESS_BAR_STYLE.bottom * scale,
			borderBottomWidth: _CARD_PROGRESS_BAR_STYLE.borderBottomWidth * scale,
			borderTopWidth: _CARD_PROGRESS_BAR_STYLE.borderTopWidth * scale,
			height: _CARD_PROGRESS_BAR_STYLE.height * scale
		};

		const progressValueStyle = {
			..._CARD_PROGRESS_VALUE_STYLE,
			width: typeof resource.progress == "number" ? Math.round(resource.progress * 100) + "%" : 0
		}

		let level = resource.level;

		switch (resource.type) {
			case "idea":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = LIGHT_BLUE;
				const idea = data.ideas.find(idea => idea.group == resource.group && idea.level == resource.level);
				title = idea.name;
				break;
			case "invention":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = DARK_PINK;
				const invention = data.ideas.find(idea => idea.group == resource.group && idea.level == resource.level);
				title = invention.name;
				break;
			case "product":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = DARK_CYAN;
				const product = data.ideas.find(idea => idea.group == resource.group && idea.level == resource.level);
				title = product.name;
				break;
			case "patent":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = DARK_ORANGE;
				const patent = data.ideas.find(idea => idea.group == resource.group && idea.level == resource.level);
				title = "\"" + patent.name + "\"";
				if (scale > 1) {
					text = capitalize(patent.patent);
				}
				break;
			case "evidence":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = BLACK;
				const evidenceResource = market.resources.find(evidenceResource => evidenceResource.id == resource.resourceId);
				if (evidenceResource) {
					const evidenceIdea = data.ideas.find(idea => idea.group == evidenceResource.group && idea.level == evidenceResource.level);
					title = evidenceIdea.name;
					level = evidenceResource.level;
				} else {
					title = "404";
				}
				break;
			case "action":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = DARK_PURPLE;
				break;
			case "lawsuit":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = DARK_RED;
				const lawsuitResource = market.resources.find(lawsuitResource => lawsuitResource.id == resource.resourceId);
				if (lawsuitResource) {
					const lawsuitIdea = data.ideas.find(idea => idea.group == lawsuitResource.group && idea.level == lawsuitResource.level);
					title = lawsuitIdea.name;
					level = lawsuitResource.level;
				} else {
					title = "404";
				}
				break;
			case "upgrade":
				bannerStyle.backgroundColor = progressValueStyle.backgroundColor = DARK_GREEN;
				break;
			default:
				bannerStyle.backgroundColor = BLACK;
				break;
		}

		const cardStyle = {
			..._CARD_STYLE,
			borderRadius: _CARD_STYLE.borderRadius * scale,
			borderWidth: _CARD_STYLE.borderWidth * scale,
			fontSize: _CARD_STYLE.fontSize * scale,
			height: _CARD_STYLE.height * scale,
			margin: _CARD_STYLE.margin * scale,
			width: _CARD_STYLE.width * scale,
			...(dragged && dragged + 1000 * 5 > new Date().valueOf() ? {transform: "translateX(-9999px)"} : {})
		}

		const levelStyle = {
			..._CARD_LEVEL_STYLE,
			height: _CARD_LEVEL_STYLE.height * scale,
			paddingTop: _CARD_LEVEL_STYLE.paddingTop * scale,
			width: _CARD_LEVEL_STYLE.width * scale
		};

		const textStyle = {
			..._CARD_TEXT_STYLE,
			...(text ? {fontSize: "50%"} : {}),
			marginTop: _CARD_TEXT_STYLE.marginTop * scale,
			padding: _CARD_TEXT_STYLE.padding * scale
		};

		return (
			<div draggable onDragEnd={onDragEnd} onDragStart={onDragStart} style={cardStyle}>
				{level ? <div style={levelStyle}>{level}</div> : null}
				<p style={textStyle}>{title}</p>
				{text ? <small style={textStyle}>{text}</small> : null}
				<div style={bannerStyle}>{capitalize(resource.type)}</div>
				{resource.progress ? <div style={progressBarStyle}>
					{typeof resource.progress == "number" ? <Fragment>
						<div style={progressValueStyle}></div>
						{resource.progress < 1 ? <div style={_CARD_PROGRESS_TEXT_STYLE}>{capitalize(resource.status) + "..."}</div> : null}
					</Fragment> : <div style={_CARD_PROGRESS_TEXT_STYLE}>{capitalize(resource.progress)}</div>}
				</div> : null}
			</div>
		);
	}
}

ResourceCard.defaultProps = {
	scale: 1
};

const handleDragEnd = function(event) {
	this.setState({dragged: null});
}

const handleDragStart = function(event) {
	const {handlers, props} = this;
	const {onDelayedDragStart} = handlers;
	const {resource} = props;

	event.dataTransfer.effectAllowed = 'move';
	event.dataTransfer.setData(resource.type, "true");
	event.dataTransfer.setData("application/json", JSON.stringify(resource));

	setTimeout(onDelayedDragStart, 1);
}

const handleDelayedDragStart = function() {
	this.setState({dragged: new Date().valueOf()});
}

const _CARD_STYLE = {
	backgroundColor: "white",
	borderColor: BLACK,
	borderStyle: "solid",
	borderWidth: 1,
	borderRadius: 5,
	fontSize: 12,
	height: 80,
	margin: 5,
	overflow: "hidden",
	pointerEvents: "all",
	position: "relative",
	width: 60
};

const _CARD_BANNER_STYLE = {
	borderTopColor: BLACK,
	borderTopStyle: "solid",
	borderTopWidth: 1,
	bottom: 0,
	color: "white",
	height: 14,
	paddingBottom: 2,
	paddingTop: 2,
	position: "absolute",
	pointerEvents: "none",
	width: "100%"
};

const _CARD_LEVEL_STYLE = {
	height: 15,
	paddingTop: 2,
	position: "absolute",
	pointerEvents: "none",
	right: 0,
	top: 0,
	width: 15
};

const _CARD_PROGRESS_BAR_STYLE = {
	backgroundColor: "#636e72",
	bottom: _CARD_BANNER_STYLE.height + _CARD_BANNER_STYLE.paddingTop + _CARD_BANNER_STYLE.paddingBottom,
	borderBottomColor: BLACK,
	borderBottomStyle: "solid",
	borderBottomWidth: 1,
	borderTopColor: BLACK,
	borderTopStyle: "solid",
	borderTopWidth: 1,
	height: 8,
	left: 0,
	pointerEvents: "none",
	position: "absolute",
	right: 0,
	width: "100%"
};

const _CARD_PROGRESS_TEXT_STYLE = {
	color: WHITE,
	fontSize: "60%",
	margin: 0,
	pointerEvents: "none",
	position: "absolute",
	top: 0,
	width: "100%"
};

const _CARD_PROGRESS_VALUE_STYLE = {
	backgroundColor: "#81ecec",
	filter: "brightness(1.2)",
	height: "100%",
	pointerEvents: "none",
	transition: "width 1s ease, background-color 1s linear",
	width: "0%"
};

const _CARD_TEXT_STYLE = {
	marginBottom: 0,
	marginTop: 14,
	opacity: 0.8,
	padding: 2,
	pointerEvents: "none"
};
