import {Component, Fragment} from "react";
import Button from "../components/Button";
import ResourceCard from "../cards/ResourceCard";
import {capitalize, durationString} from "../util/Util"
import {BUTTON_STYLE, HEADER_STYLE} from "../style/CSS"

export default class LawsuitCard extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onChangeMultiplier: handleChangeMultiplier.bind(this),
			onChangeProgress: handleChangeProgress.bind(this),
            onDismiss: handleDismiss.bind(this),
            onGoToTrial: handleGoToTrial.bind(this),
            onSettle: handleSettle.bind(this)
		}

		this.state = {multiplier: 1};
	}

	render() {
        const {handlers, props, state} = this;
        const {onChangeMultiplier, onDismiss, onGoToTrial, onSettle} = handlers;
        const {data, lawsuit, market, scale} = props;
		const {multiplier} = state;

        const plaintiff = market.teams.find(team => team.id == lawsuit.teamId);
        const infringement = market.resources.find(resource => resource.id == lawsuit.resourceId);

        let patent;
        if (plaintiff && infringement) {
            // Give the plaintiff the benefit of the doubt by finding the earliest valid (TODO) patent
            const patents = market.resources.filter(resource => resource.teamId == plaintiff.id && resource.type == "patent" && resource.group == infringement.group && resource.level == infringement.level);
            patents.sort((a, b) => a.created - b.created);
            patent = patents[0];
        }

        let defendant;
        if (infringement) {
            defendant = market.teams.find(team => team.id == infringement.teamId);
        }

        const cardProps = {
            data,
            market,
            scale
        }

        return (<div style={_CARD_STYLE}>
            <h3 style={HEADER_STYLE}>{`${plaintiff ? plaintiff.name : "Unknown"} vs. ${defendant ? defendant.name : "Unkown"}`}</h3>
            {lawsuit.progress ? <p>{capitalize(lawsuit.progress || "invalid")}</p> : null}
            <p>{data.lawsuitStrategies.find(lawsuitStrategy => lawsuitStrategy.id == (plaintiff && plaintiff.lawsuitStrategy ? plaintiff.lawsuitStrategy : "compensate")).header}</p>
            {lawsuit.status == "trialing" ? <Fragment>
                <table>
                    {patent ? <tr>
                        <td>
                            <ResourceCard key={patent.id} resource={patent} {...cardProps}/>
                        </td>
                        <td>
                            <h6>Exhibit A</h6>
                            <ul>
								{defendant ? <li>Issued to {plaintiff.name}</li> : null}
                                <li>Created {durationString(patent.created)} ago</li>
                                <li>Issued {durationString(patent.issued)} ago</li>
                            </ul>
                        </td>
                    </tr> : null}
                    {infringement ? <tr>
                        <td>
                            <ResourceCard key={infringement.id} resource={infringement} {...cardProps}/>
                        </td>
                        <td>
                            <h6>Exhibit {patent ? "B" : "A"}</h6>
                            <ul>
								{defendant ? <li>Produced by {defendant.name}</li> : null}
                                <li>Created {durationString(infringement.created)} ago</li>
                                <li>Produced {durationString(infringement.produced)} ago</li>
                            </ul>
                        </td>
                    </tr> : null}
                </table>
                {lawsuit.progress == "jury" ? <Fragment>
					<input name="multiplier" type="range" value={multiplier} onChange={onChangeMultiplier} min="0" max="1" step="0.01" style={_SLIDER_STYLE}/>
					<p>{Math.round(multiplier * 100)}% statutory damages</p>
					<p>{Math.round(multiplier * 500)}% punitive damages</p>
					<br/>
					<select disabled={!patent || !infringement} onChange={onSettle} value={"null"} style={BUTTON_STYLE}>
						<option value={"null"} disabled={true}>Settle</option>
						{data.lawsuitStrategies.map(lawsuitStrategy => <option value={lawsuitStrategy.id}>{lawsuitStrategy.decision}</option>)}
					</select>
                    <Button onClick={onDismiss} text="Dismiss"/>
                </Fragment> : null}
            </Fragment> : <Button onClick={onGoToTrial} text="Go to trial"/>}
        </div>);
	}
}

ResourceCard.defaultProps = {
	scale: 1
};

const handleChangeMultiplier = function(event) {
	const multiplier = event.target.value;
	this.setState({multiplier});
};

const handleChangeProgress = function(value, extras) {
    const {props} = this;
    const {lawsuit, onActionOnResource} = props;

    onActionOnResource(lawsuit.id, {type: "changeProgress", value, ...(extras || {})})
};

const handleDismiss = function(event) {
    const {handlers} = this;
    const {onChangeProgress} = handlers;

    onChangeProgress("dismissed");
};

const handleGoToTrial = function(event) {
    const {props} = this;
    const {lawsuit, onActionOnResource} = props;

    onActionOnResource(lawsuit.id, {type: "move", status: "trialing"})
}

const handleSettle = function(event) {
    const {handlers, multiplier} = this;
    const {onChangeProgress} = handlers;
	const decision = event.target.value;

	if (decision !== "null") {
		onChangeProgress("settled", {decision, multiplier});
	}
};

const _CARD_STYLE = {
    backgroundColor: "white",
    borderRadius: 10,
    display: "inline",
    margin: 5,
    padding: 5
};

const _SLIDER_STYLE = {
	pointerEvents: "all"
};
