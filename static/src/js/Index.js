import {render} from "react-dom";
import {Fragment, PureComponent} from "react";
import axios from "axios";
import AuthenticatedPage from "pages/AuthenticatedPage";
import UnauthenticatedPage from "pages/UnauthenticatedPage";

class AuthenticatedApp extends PureComponent {
	constructor() {
		super(...arguments);

		this.handlers = {
			onCredentials: handleCredentials.bind(this),
			onEditRequest: handleEditRequest.bind(this),
			onLogin: handleLogin.bind(this),
			onLogout: handleLogout.bind(this),
			onReadRequest: handleReadRequest.bind(this),
			onRegister: handleRegister.bind(this)
		}

		let {handlers} = this;
		let {onCredentials} = handlers

		this.url = "https://" + window.location.hostname + "/";

		if (window.location.host.includes("patent")) {
			document.title = "PatentWarz";
		}

		axios.get("/cognito.json").then(response => {
			this.pool = new AmazonCognitoIdentity.CognitoUserPool({
				UserPoolId : response.data.userPoolId,
				ClientId : response.data.userPoolClientId
			});

			let user = this.pool.getCurrentUser();

			if (user) {
				user.getSession(function(err, session) {
					if (err) {
						console.log(err);
					}

					if (session.isValid()){
						onCredentials(localStorage.getItem("code"), user, session);
					}
				});
			}
		});

		this.state = {session: null, user: null};
	}

	render() {
		const {handlers, state} = this;
		const {onLogin, onEditRequest, onLogout, onRegister, onReadRequest} = handlers;
		const {marketId, session, user} = state;

		return (
			<Fragment>
				{user && session ? <AuthenticatedPage onEditRequest={onEditRequest} username={user.username} marketId={marketId} onLogout={onLogout} onReadRequest={onReadRequest}/> : <UnauthenticatedPage onLogin={onLogin} onRegister={onRegister}/>}
			</Fragment>
		);
	}
}

const app = document.getElementById("app");
render(<AuthenticatedApp/>, app);

function handleCredentials(marketId, user, session) {
	this.setState({marketId, user, session});
}

function handleLogin(code, username, password, callback) {
	let {handlers} = this;
	let {onCredentials} = handlers;

	var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
		Username: username,
		Password: password
	});

	var cognitoUser = new AmazonCognitoIdentity.CognitoUser({
		Username: username,
		Pool: this.pool
	});

	function handleLoginSuccess(result) {
		onCredentials(code, cognitoUser, result);
	}

	cognitoUser.authenticateUser(authenticationDetails, {
		onSuccess: handleLoginSuccess.bind(this),
		onFailure: function(err) {
			callback(err);
		}
	});
}

function handleLogout() {
	const {handlers, state} = this;
	const {onCredentials} = handlers;
	const {marketId, user} = state;
	user.globalSignOut({
		onSuccess: function(result) {
			onCredentials(marketId, null, null);
		}, onFailure: function(err) {
			console.log(err);
		}
	});
}

function handleRegister(username, password, callback) {
	function handleRegisterCallback(err, result) {
		if (err) {
			callback(err);
		} else {
			this.setState({user: result.user});
			callback(null);
		}
	}
	this.pool.signUp(username, password, [], null, handleRegisterCallback.bind(this));
}

function handleReadRequest(path, callback) {
	const {setState, state} = this;
	const {marketId, session} = state;

	const setThisState = setState.bind(this);

	if (session) {
		axios.get(`/api/market/${marketId}` + path, {
			headers: {
				"Authorization": `Bearer ${session.idToken.jwtToken}`
			}
		}).then(response => {
			callback(null, response.data);
		}).catch(err => {
			if (err.response && err.response.status == 401) {
				setThisState({session: null});
			}
			callback(err);
		});
	}
}

function handleEditRequest(path, data, callback) {
	const {setState, state} = this;
	const {marketId, session} = state;

	const setThisState = setState.bind(this);

	if (session) {
		axios.post(`/api/market/${marketId}` + path, data, {
			headers: {
				'Authorization': `Bearer ${session.idToken.jwtToken}`
			}
		}).then(response => {
			callback(null, response.data);
		}).catch(err => {
			if (err.response && err.response.status == 401) {
				setThisState({session: null});
			}
			callback(err);
		});
	}
}
