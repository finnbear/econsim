import {Component} from "react";
import MoneyPanel from "../panels/MoneyPanel";
import ResourcesPanel from "../panels/ResourcesPanel";
import {BUTTON_STYLE, CONTAINER_STYLE, DIALOG_STYLE, FORM_STYLE, HEADER_STYLE, INPUT_STYLE, MESSAGE_STYLE, ERROR_STYLE, PAGE_STYLE, RED_STYLE, TABLE_STYLE, TABLE_DATA_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"

export default class InventorScreen extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onChangeInventionStrategy: handleChangeInventionStrategy.bind(this)
		};

		this.state = {};
	}

	static getDerivedStateFromProps(props, prevState) {
		const {market} = props;
		const {resources} = market;

		const roleName = "inventor";

		const team = market.teams.find(team => team.id == market.teamId);

		let inputBudget = team.inventorBudget;
		const inputIdeas = [];
		const inputActions = [];

		const inventingResources = [];
		const inventionStrategy = team.inventionStrategy;

		const outputProductInventions = [];
		const outputPatentInventions = [];
		const outputFiles = [];

		for (let resource of resources.filter(resource => resource.teamId == team.id)) {
			switch (resource.type) {
				case "idea":
					switch (resource.status) {
						case "transferring":
							if (resource.input == roleName) {
								inputIdeas.push(resource);
							} else if (resource.output == roleName) {
								outputFiles.push(resource);
							}
							break;
						case "inventing":
							inventingResources.push(resource);
							break;
					}
					break;
				case "invention":
					switch (resource.status) {
						case "transferring":
							if (resource.output == roleName) {
								switch (resource.input) {
									case "marketer":
										outputProductInventions.push(resource);
										break;
									case "lawyer":
										outputPatentInventions.push(resource);
										break;
									case null:
									case undefined:
										outputFiles.push(resource);
										break;
								}
							}
							break
						case "inventing":
							inventingResources.push(resource);
							break;
					}
					break;
				case "action":
					if (resource.input == roleName) {
						inputActions.push(resource);
					} else if (resource.output == roleName) {
						outputFiles.push(action);
					}
					break;
			}
		}

		return {inputBudget, inputIdeas, inputActions, inventingResources, inventionStrategy, outputProductInventions, outputPatentInventions, outputFiles};
	}


	render() {
		const {handlers, props, state} = this;
		const {onChangeInventionStrategy} = handlers;
		const {data, onActionOnResource} = props;
		const {inputBudget, inputIdeas, inputActions, inventingResources, inventionStrategy, outputProductInventions, outputPatentInventions, outputFiles} = state;

		const panelProps = {
			data,
			onActionOnResource
		};

		return (
			<table style={TABLE_STYLE}>
				<tbody>
					<tr>
						<MoneyPanel name="Budget" value={inputBudget}/>
						<ResourcesPanel name="Invention Lab" resources={inventingResources} filter={"idea"} action={{type: "move", status: "inventing"}} colSpan={2} rowSpan={2} onMessage={handleMessageInventionLab}{...panelProps}/>
						<ResourcesPanel name="Future Products" resources={outputProductInventions} filter={"invention"} action={{type: "move", output: "inventor", input: "marketer"}} {...panelProps}/>
					</tr>
					<tr>
						<ResourcesPanel name="Ideas" resources={inputIdeas} filter={false} {...panelProps}/>
						<ResourcesPanel name="Future Patents" resources={outputPatentInventions} filter={"invention"} action={{type: "move", output: "inventor", input: "lawyer"}} {...panelProps}/>
					</tr>
					<tr>
						<ResourcesPanel name="Actions" resources={inputActions} filter={false} {...panelProps}/>
						<td colSpan="2" style={TABLE_DATA_STYLE}>
							<h2 style={HEADER_STYLE}>Invention Strategy</h2>
							<select onChange={onChangeInventionStrategy} value={inventionStrategy} style={BUTTON_STYLE}>
								{data.inventionStrategies.map((inventionStrategy, index) => <option disabled={index > 0 && inputBudget < inventionStrategy.cost} value={inventionStrategy.id}>{inventionStrategy.name} (${inventionStrategy.cost}/idea)</option>)}
							</select>
						</td>
						<ResourcesPanel name="File Cabinet" resources={outputFiles} filter={true} action={{type: "move", output: "inventor", input: null}} maxCards={0} {...panelProps}/>
					</tr>
				</tbody>
			</table>
		);
	}
}

const handleChangeInventionStrategy = function(event) {
	const {props} = this;
	const {onActionOnTeam} = props;
	const value = event.target.value;
	onActionOnTeam(null, {type: "changeInventionStrategy", value});
}

const handleMessageInventionLab = function(resources) {
	if (resources.length == 0) {
		return "Drag an idea card here to invent it";
	}
}
