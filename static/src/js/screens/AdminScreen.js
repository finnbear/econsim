import {Component, Fragment} from "react";
import CourtPanel from "../panels/CourtPanel";
import MarketPanel from "../panels/MarketPanel";
import Player from "../avatars/Player.js";
import Button from "../components/Button";
import {capitalize, sortByName} from "../util/Util.js"
import {BUTTON_STYLE, CONTAINER_STYLE, DIALOG_STYLE, FORM_STYLE, HEADER_STYLE, INPUT_STYLE, MESSAGE_STYLE, ERROR_STYLE, PAGE_STYLE, RED_STYLE, TABLE_STYLE, TABLE_DATA_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"

export default class AdminScreen extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onChangeSortTeams: handleChangeSortTeams.bind(this),
			onChangeUserRole: handleChangeUserRole.bind(this),
			onChangeUserTeam: handleChangeUserTeam.bind(this),
			onCreateTeam: handleCreateTeam.bind(this),
			onCreateTeams: handleCreateTeams.bind(this),
			onDeleteEmptyTeams: handleDeleteEmptyTeams.bind(this),
			onEndGame: handleChangeGameStatus("ended").bind(this),
			onOpenGame: handleChangeGameStatus("open").bind(this),
			onPauseGame: handleChangeGameStatus("paused").bind(this),
			onStartGame: handleChangeGameStatus("started").bind(this)
		}

		this.state = {};
	}

	static getDerivedStateFromProps(props) {
		const {data, market} = props;

		const administeringTeams = [];
		const administeringUsers = [];

		if (market) {
			const {teams, users} = market;

			for (let team of teams) {
				const administeringTeam = {...team, full: true};

				const administeringTeamUsers = users.filter(user => user.teamId && user.teamId == team.id);

				for (let role of data.roles) {
					const user = administeringTeamUsers.find(user => user.roles.includes(role));

					if (user) {
						administeringTeam[role] = user;
					} else {
						administeringTeam.full = false;
					}
				}

				administeringTeams.push(administeringTeam);
			}

			for (let user of users) {
				const administeringUser = {...user};
				administeringUsers.push(administeringUser);
			}
		}

		return {administeringTeams, administeringUsers}
	}

	render() {
		const {handlers, props, state} = this;
		const {onChangeSortTeams, onChangeUserRole, onChangeUserTeam, onCreateTeam, onCreateTeams, onDeleteEmptyTeams, onEndGame, onOpenGame, onPauseGame, onStartGame} = handlers;
		const {data, market, onActionOnResource} = props;
		const {administeringTeams, administeringUsers, sortTeams} = state;

		sortByName(administeringTeams, sortTeams);
		sortByName(administeringUsers);

		const panelProps = {
			data,
			market,
			onActionOnResource
		};

		return (
			<table style={TABLE_STYLE}>
			 	<tbody>
					<tr>
						<td style={TABLE_DATA_STYLE}>
							<h2 style={HEADER_STYLE}>Players</h2>
							<Button disabled={market.status != "open"} onClick={onCreateTeams} text="Randomize&trade;" style={BUTTON_STYLE}/>
						</td>
						<td style={TABLE_DATA_STYLE}>
							<h2 style={HEADER_STYLE}>Game</h2>
							<p>{capitalize(market.status)}</p>
							{["open", "paused", "ended"].includes(market.status) ? <button onClick={onStartGame} style={BUTTON_STYLE}>{market.status == "paused" ? "Unpause " : "Start "} Game</button> : null}
							{["paused"].includes(market.status) ? <button onClick={onOpenGame} style={BUTTON_STYLE}>Open Game</button> : null}
							{["started"].includes(market.status) ? <button onClick={onPauseGame} style={BUTTON_STYLE}>Pause Game</button> : null}
							{["started"].includes(market.status) ? <button onClick={onEndGame} style={BUTTON_STYLE}>End Game</button> : null}
						</td>
						<td style={TABLE_DATA_STYLE}>
							<h2 style={HEADER_STYLE}>Dividend</h2>
							<button style={BUTTON_STYLE}>Give $5 to All Teams</button>
						</td>
					</tr>
					<tr>
						<MarketPanel {...panelProps} colSpan="3"/>
					</tr>
					<tr>
						<CourtPanel {...panelProps} colSpan="3"/>
					</tr>
					<tr>
						<td colSpan="3" style={TABLE_DATA_STYLE}>
							<h2 style={HEADER_STYLE}>Teams ({administeringTeams.length})</h2>
							<table style={TABLE_STYLE}>
								<thead>
									<tr>
										<th>Team Name</th>
										{data.roles.map(role => <th>{capitalize(role)}</th>)}
										<th>Total Balance</th>
									</tr>
								</thead>
								<tbody>
									{administeringTeams.map(team => <tr key={team.id}>
										<td>{team.name}</td>
										{data.roles.map(role => <td>{role in team ? team[role].name : "-"}</td>)}
										<td>${team.balance + team.inventorBudget + team.marketerBudget + team.lawyerBudget}</td>
									</tr>)}
								</tbody>
							</table>
							<button onClick={onCreateTeam} style={BUTTON_STYLE}>Create Team</button>
							<button onClick={onDeleteEmptyTeams} style={BUTTON_STYLE}>Delete Empty Teams</button>
							<select onChange={onChangeSortTeams} value={sortTeams} style={BUTTON_STYLE}>
								{["name", "balance"].map(sortOption => <option value={sortOption}>{`Sort by ${sortOption}`}</option>)}
							</select>
						</td>
					</tr>
					<tr>
						<td colSpan="3" style={TABLE_DATA_STYLE}>
							<h2 style={HEADER_STYLE}>Players ({administeringUsers.length})</h2>
							<table style={TABLE_STYLE}>
								<thead>
									<tr>
										<th>Username</th>
										<th>Team</th>
										<th>Role(s)</th>
									</tr>
								</thead>
								<tbody>
									{administeringUsers.map(user => <tr key={user.id}>
										<td>{user.name}</td>
										<td>
											<select onChange={onChangeUserTeam} value={user.id + (user.teamId ? ">" + user.teamId : "")} style={BUTTON_STYLE}>
												<option value={user.id}>No Team</option>
												{administeringTeams.map(team => <option value={user.id + ">" + team.id}>{team.name + (team.full ? " (FULL)" : "")}</option>)}
											</select>
										</td>
										<td>
											<select onChange={onChangeUserRole} value={user.id} style={BUTTON_STYLE}>
												<option disabled value={user.id}>{user.roles.length > 0 ? user.roles.join(", ") : "None"}</option>
												{data.roles.map(role => <option value={user.id + (user.roles.includes(role) ? "<" : ">") + role}>{(user.roles.includes(role) ? "Remove " : "Add ") + role + " role"}</option>)}
											</select>
										</td>
									</tr>)}
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		);
	}
}

const handleChangeGameStatus = function(value) {
	return function() {
		const {props} = this;
		const {onActionOnMarket} = props;

		onActionOnMarket({type: "changeStatus", value});
	}
}

const handleChangeSortTeams = function(event) {
	const sortTeams = event.target.value;
	this.setState({sortTeams});
}

const handleChangeUserRole = function(event) {
	const {props} = this;
	const {onActionOnUser} = props;
	const value = event.target.value;

	if (value.includes(">")) {
		const segments = value.split(">");
		onActionOnUser(segments[0], {type: "addRole", value: segments[1]});
	} else if (value.includes("<")) {
		const segments = value.split("<");
		onActionOnUser(segments[0], {type: "removeRole", value: segments[1]});
	}
}

const handleChangeUserTeam = function(event) {
	const {props} = this;
	const {onActionOnUser} = props;
	const segments = event.target.value.split(">");
	onActionOnUser(segments[0], {type: "changeTeam", value: segments[1] || null});
}

const handleCreateTeam = function(event) {
	const {props} = this;
	const {onActionOnTeam} = props;
	onActionOnTeam("", {type: "create"});
}

const handleCreateTeams = function(event) {
	const {props} = this;
	const {onActionOnMarket} = props;
	onActionOnMarket({type: "createTeams"});
}

const handleDeleteEmptyTeams = function(event) {
	const {props, state} = this;
	const {data, market, onActionOnTeam} = props;
	const {administeringTeams} = state;
	for (let team of administeringTeams) {
		let empty = true;
		for (let role of data.roles) {
			if (role in team) {
				empty = false;
			}
		}

		if (empty) {
			onActionOnTeam(team.id, {type: "delete"});
		}
	}
}
