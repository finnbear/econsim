import {Component} from "react";
import MoneyPanel from "../panels/MoneyPanel";
import ResourcesPanel from "../panels/ResourcesPanel";
import Player from "../avatars/Player.js";
import {BUTTON_STYLE, CONTAINER_STYLE, DIALOG_STYLE, FORM_STYLE, HEADER_STYLE, INPUT_STYLE, MESSAGE_STYLE, ERROR_STYLE, PAGE_STYLE, RED_STYLE, TABLE_STYLE, TABLE_DATA_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"

export default class EntrepreneurScreen extends Component {
	constructor(props) {
		super(...arguments);

		this.state = {};
	}

	static getDerivedStateFromProps(props, prevState) {
		const {market} = props;
		const {resources} = market;

		const roleName = "entrepreneur";

		const team = market.teams.find(team => team.id == market.teamId);

		let inputBalance = team.balance;
		const inputActions = [];
		const inputUpgrades = [];
		let outputInventorBudget = team.inventorBudget;
		let outputMarketerBudget = team.marketerBudget;
		let outputLawyerBudget = team.lawyerBudget;

		for (let resource of resources.filter(resource => resource.teamId == team.id)) {
			switch (resource.type) {
				case "action":
					if (resource.input == roleName) {
						inputActions.push(resource);
					}
					break;
				case "upgrade":
					if (resource.input == roleName) {
						inputUpgrades.push(resource);
					}
					break;
			}
		}

		return {inputBalance, inputActions, inputUpgrades, outputInventorBudget, outputMarketerBudget, outputLawyerBudget};
	}


	render() {
		const {props, state} = this;
		const {onActionOnTeam} = props;
		const {inputBalance, inputActions, inputUpgrades, outputInventorBudget, outputMarketerBudget, outputLawyerBudget} = state;

		const tableStyle = {
			...TABLE_STYLE,
			borderSpacing: 0
		};

		const playerProps = {
			onActionOnTeam
		};

		return (
			<table style={TABLE_STYLE}>
				<tbody>
					<tr>
						<MoneyPanel name="Balance" value={inputBalance}/>
						<td colSpan="2" rowSpan="4" style={TABLE_DATA_DRAG_STYLE}>
							<h2 style={HEADER_STYLE}>Accounting Office</h2>
							<table style={tableStyle}>
								<tbody>
									<tr>
										<td/>
										<Player role="entrepreneur"/>
										<td/>
									</tr>
									<tr>
										<Player role="inventor" balance={inputBalance} {...playerProps}/>
										<Player role="marketer" balance={inputBalance} {...playerProps}/>
										<Player role="lawyer" balance={inputBalance} {...playerProps}/>
									</tr>
								</tbody>
							</table>
						</td>
						<MoneyPanel name="Inventor Budget" value={outputInventorBudget}/>
					</tr>
					<tr>
						<ResourcesPanel name="Actions" resources={inputActions} filter={false}/>
						<MoneyPanel name="Marketer Budget" value={outputMarketerBudget}/>
					</tr>
					<tr>
						<ResourcesPanel name="Upgrades" resources={inputUpgrades} filter={false}/>
						<MoneyPanel name="Lawyer Budget" value={outputLawyerBudget}/>
					</tr>
				</tbody>
			</table>
		);
	}
}
