import {Component} from "react";
import MoneyPanel from "../panels/MoneyPanel";
import TextPanel from "../panels/TextPanel";
import ResourcesPanel from "../panels/ResourcesPanel";
import Player from "../avatars/Player.js";
import Lawyer from "../avatars/Lawyer.js";
import {BUTTON_STYLE, CONTAINER_STYLE, DIALOG_STYLE, FORM_STYLE, HEADER_STYLE, INPUT_STYLE, MESSAGE_STYLE, ERROR_STYLE, PAGE_STYLE, RED_STYLE, TABLE_STYLE, TABLE_DATA_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"

export default class LawyerScreen extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onChangeLawsuitStrategy: handleChangeLawsuitStrategy.bind(this)
		};

		this.state = {};
	}

	static getDerivedStateFromProps(props, prevState) {
		const {market} = props;
		const {resources} = market;

		const roleName = "lawyer";

		const team = market.teams.find(team => team.id == market.teamId);

		let inputBudget = team.lawyerBudget;
		const inputInventions = [];
		const inputEvidence = [];
		const inputActions = [];
		const inputUpgrades = [];

		const {lawsuitStrategy} = team;
		const patentingResources = [];
		const examiningResources = [];
		const holdingResources = [];

		let outputWinnings = team.lawyerWinnings || 0;
		let outputLegalPower = 0;
		const outputLawsuits = [];
		const outputFiles = [];

		for (let resource of resources.filter(resource => resource.teamId == team.id)) {
			switch (resource.type) {
				case "invention":
					switch (resource.status) {
						case "transferring":
							if (resource.input == roleName) {
								inputInventions.push(resource);
							} else if (resource.output == roleName) {
								outputFiles.push(resource);
							}
							break;
						case "patenting":
							patentingResources.push(resource);
							break;
					}
					break;
				case "evidence":
					if (resource.input == roleName) {
						inputEvidence.push(resource);
					} else if (resource.status == "examining") {
						examiningResources.push(resource);
					} else if (resource.output == roleName) {
						outputFiles.push(resource);
					}
					break;
				case "lawsuit":
					if (resource.status == "examining") {
						examiningResources.push(resource);
					} else if (resource.output == roleName) {
						if (resource.input) {
							outputLawsuits.push(resource);
						} else {
							outputFiles.push(resource);
						}
					}
					break;
				case "lawyer":
					outputLegalPower += resource.level;
					break;
				case "action":
					if (resource.input == roleName) {
						inputActions.push(resource);
					} else if (resource.output == roleName) {
						outputFiles.push(resource);
					}
					break;
				case "upgrade":
					if (resource.input == roleName) {
						inputUpgrades.push(resource);
					} else if (resource.output == roleName) {
						outputFiles.push(resource);
					}
					break;
				case "patent":
					switch (resource.status) {
						case "transferring":
							outputFiles.push(resource);
							break;
						case "patenting":
							patentingResources.push(resource);
							break;
						case "examining":
							examiningResources.push(resource);
							break;
						case "holding":
							holdingResources.push(resource);
							break;
					}
					break;
			}
		}

		return {inputBudget, inputInventions, inputEvidence, inputActions, inputUpgrades, lawsuitStrategy, patentingResources, examiningResources, holdingResources, outputWinnings, outputLegalPower, outputLawsuits, outputFiles};
	}


	render() {
		const {handlers, props, state} = this;
		const {onChangeLawsuitStrategy} = handlers;
		const {data, market, onActionOnMarket, onActionOnTeam, onActionOnResource} = props;
		const {inputBudget, inputInventions, inputEvidence, inputActions, inputUpgrades, lawsuitStrategy, patentingResources, examiningResources, holdingResources, outputWinnings, outputLegalPower, outputLawsuits, outputFiles} = state;

		const tableStyle = {
			...TABLE_STYLE,
			borderSpacing: 0
		};

		const panelProps = {
			data,
			market,
			onActionOnResource
		};

		const lawyerProps = {
			data,
			onActionOnTeam
		};

		return (
			<table style={TABLE_STYLE}>
				<tbody>
					<tr>
						<MoneyPanel name="Budget" value={inputBudget}/>
						<td colSpan="2" rowSpan="2" style={TABLE_DATA_STYLE}>
							<h2 style={HEADER_STYLE}>Law Firm</h2>
							<table style={tableStyle}>
								<tbody>
									<tr>
										<td/>
										<Player role="lawyer"/>
										<td/>
									</tr>
									<tr>
										{[1,2,3].map(level => <Lawyer key={level} level={level} budget={inputBudget} {...lawyerProps}/>)}
									</tr>
								</tbody>
							</table>
							<select onChange={onChangeLawsuitStrategy} value={lawsuitStrategy} style={BUTTON_STYLE}>
								{data.lawsuitStrategies.map((lawsuitStrategy, index) => <option value={lawsuitStrategy.id}>{lawsuitStrategy.name}</option>)}
							</select>
						</td>
						<MoneyPanel name="Winnings" value={outputWinnings}/>
					</tr>
					<tr>
						<ResourcesPanel name="Actions" resources={inputActions} filter={false} {...panelProps}/>
						<TextPanel name="Legal Power" value={outputLegalPower}/>
					</tr>
					<tr>
						<ResourcesPanel name="Evidence" resources={inputEvidence} filter={false} {...panelProps}/>
						<ResourcesPanel name="Legal Office" resources={examiningResources}  action={{type: "move", status: "examining"}} filter={["evidence", "patent"]} colSpan={2} onMessage={(resources) => {
							if (resources.length == 0) {
								return "Drag a patent card and an evidence card here"
							} else {
								const patent = resources.find(resource => resource.type == "patent");
								const evidenceOrLawsuit = resources.find(resource => ["evidence", "lawsuit"].includes(resource.type));

								if (!patent) {
									return "Drag a relevant patent card here";
								} else if (!evidenceOrLawsuit) {
									return "Drag a relevant evidence card here";
								} else {
									// Needs a reference to the market
									const referencedResource = market.resources.find(resource => resource.id == evidenceOrLawsuit.resourceId);

									if (!referencedResource) {
										return "Evidence card has expired";
									} else if (referencedResource.group != patent.group) {
										return "Patent card does not apply";
									} else if (referencedResource.level > patent.level) {
										return "Patent card level is too low to apply";
									} else if (referencedResource.level < patent.level) {
										return "Patent card level is too high to apply";
									}
								}
							}
						}} {...panelProps}/>
						<ResourcesPanel name="Lawsuits" resources={outputLawsuits} action={{type: "move", output: "lawyer", input: "game"}} filter={"lawsuit"} {...panelProps}/>
					</tr>
					<tr>
						<ResourcesPanel name="Inventions" resources={inputInventions} filter={false} {...panelProps}/>
						<ResourcesPanel name="Patent Office" resources={patentingResources} filter={"invention"} action={{type: "move", status: "patenting"}} colSpan={2} onMessage={handleMessagePatentOffice} {...panelProps}/>
						<ResourcesPanel name="File Cabinet" resources={outputFiles} action={{type: "move", output: "lawyer", input: null}} filter={true} maxCards={0} {...panelProps}/>
					</tr>
					<tr>
						<ResourcesPanel name="Issued Patents" resources={holdingResources} action={{type: "move", status: "holding"}} filter={"patent"} colSpan={4} maxCards={0} {...panelProps}/>
					</tr>
				</tbody>
			</table>
		);
	}
}

const handleChangeLawsuitStrategy = function(event) {
	const {props} = this;
	const {onActionOnTeam} = props;
	onActionOnTeam(null, {type: "changeLawsuitStrategy", value: event.target.value});
}

const handleMessagePatentOffice = function(resources) {
	if (resources.length == 0) {
		return "Drag an invention here card to patent it";
	}
}
