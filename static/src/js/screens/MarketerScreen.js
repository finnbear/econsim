import {Component} from "react";
import MarketPanel from "../panels/MarketPanel";
import MoneyPanel from "../panels/MoneyPanel";
import ResourcesPanel from "../panels/ResourcesPanel";
import Button from "../components/Button";
import {BUTTON_STYLE, CONTAINER_STYLE, DIALOG_STYLE, FORM_STYLE, HEADER_STYLE, INPUT_STYLE, MESSAGE_STYLE, ERROR_STYLE, PAGE_STYLE, RED_STYLE, TABLE_STYLE, TABLE_DATA_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"

export default class MarketerScreen extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onAddEvidence: handleAddEvidence.bind(this),
			onAddInspiration: handleAddInspiration.bind(this),
			onSetParentState: handleSetParentState.bind(this)
		};

		this.state = {};
	}

	static getDerivedStateFromProps(props, prevState) {
		const {market} = props;
		const {resources} = market;

		const roleName = "marketer";

		const team = market.teams.find(team => team.id == market.teamId);

		let inputBudget = team.marketerBudget;
		const inputInventions = [];
		const inputActions = [];
		const inputUpgrades = [];

		const producingResources = [];
		const marketingResources = [];

		let outputRevenue = team.marketerRevenue;
		const outputProducts = [];
		const outputIdeas = [];
		const outputEvidence = [];
		const outputFiles = [];

		for (let resource of resources.filter(resource => resource.teamId == team.id)) {
			switch (resource.type) {
				case "idea":
					if (resource.output == roleName) {
						if (!resource.input) {
							outputFiles.push(resource);
						} else {
							outputIdeas.push(resource);
						}
					}
					break;
				case "invention":
					switch (resource.status) {
						case "transferring":
							if (resource.input == roleName) {
								inputInventions.push(resource);
							} else if (resource.output == roleName) {
								outputFiles.push(resource);
							}
							break;
						case "producing":
							producingResources.push(resource);
							break;
					}
					break;
				case "product":
					switch (resource.status) {
						case "transferring":
							if (resource.output == roleName) {
								if (resource.input == "lawyer") {
									outputEvidence.push(resource);
								} else {
									outputFiles.push(resource);
								}
							}
							break;
						case "producing":
							producingResources.push(resource);
							break;
						case "marketing":
							marketingResources.push(resource);
							break;
					}
					break;
				case "evidence":
					if (resource.status == "transferring" && resource.output == roleName) {
						if (resource.input) {
							outputEvidence.push(resource);
						} else {
							outputFiles.push(resource);
						}
					}
					break;
				case "action":
					if (resource.input == roleName) {
						inputActions.push(resource);
					} else if (resource.output == roleName) {
						outputFiles.push(resource);
					}
					break;
				case "upgrade":
					if (resource.input == roleName) {
						inputUpgrades.push(resource);
					} else if (resource.output == roleName) {
						outputFiles.push(resource);
					}
					break;
			}
		}

		return {inputBudget, inputInventions, inputActions, inputUpgrades, producingResources, marketingResources, outputRevenue, outputProducts, outputIdeas, outputEvidence, outputFiles};
	}


	render() {
		const {handlers, props, state} = this;
		const {onAddEvidence, onAddInspiration, onSetParentState} = handlers;
		const {data, market, onActionOnResource} = props;
		const {inputBudget, inputInventions, inputActions, inputUpgrades, producingResources, marketingResources, outputRevenue, outputProducts, outputIdeas, outputEvidence, outputFiles, selectionId} = state;

		const panelProps = {
			data,
			market,
			onActionOnResource
		};

		const marketPanelProps = {
			...panelProps,
			onSetParentState,
			selectionId
		};

		const selection = market.resources.find(resource => resource.id == selectionId);

		return (
			<table style={TABLE_STYLE}>
				<tbody>
					<tr>
						<MoneyPanel name="Budget" value={inputBudget}/>
						<MarketPanel market={market} action={{type: "move", status: "marketing"}} filter={"product"} colSpan="2" rowSpan="3" {...marketPanelProps}/>
						<MoneyPanel name="Revenue" value={outputRevenue}/>
					</tr>
					<tr>
						<ResourcesPanel name="Actions" resources={inputActions} filter={false} {...panelProps}/>
						<ResourcesPanel name="Inspiration" resources={outputIdeas} action={{type: "move", output: "marketer", input: "inventor"}} filter={"idea"} {...panelProps}>
							<Button onClick={onAddInspiration} disabled={!selectionId} text="Add Selection ($5)"/>
						</ResourcesPanel>
					</tr>
					<tr>
						<ResourcesPanel name="Upgrades" resources={inputUpgrades} filter={false} {...panelProps}/>
						<ResourcesPanel name="Evidence" resources={outputEvidence} action={{type: "move", output: "marketer", input: "lawyer"}} filter={"evidence"} maxCards={2} {...panelProps}>
							<Button onClick={onAddEvidence} disabled={!selection || inputBudget < 5 || selection.teamId == market.teamId || outputEvidence.length > 2} text="Add Selection ($5)"/>
						</ResourcesPanel>
					</tr>
					<tr>
						<ResourcesPanel name="Inventions" resources={inputInventions} filter={false} {...panelProps}/>
						<ResourcesPanel name="Factory" resources={producingResources} action={{type: "move", status: "producing"}} filter={"invention"} colSpan={2} onMessage={handleMessageFactory} {...panelProps}/>
						<ResourcesPanel name="File Cabinet" resources={outputFiles} action={{type: "move", output: "marketer", input: null}} filter={true} maxCards={0} {...panelProps}/>
					</tr>
					<tr>
						<ResourcesPanel name="Products on the Market" resources={marketingResources.filter(resource => resource.teamId == market.teamId)} action={{type: "move", status: "marketing"}} filter={"product"} colSpan={4} maxCards={0} {...panelProps}/>
					</tr>
				</tbody>
			</table>
		);
	}
}

const handleAddEvidence = function() {
	const {props, state} = this;
	const {onActionOnResource} = props;
	const {selectionId} = state;
	if (selectionId) {
		onActionOnResource(selectionId, {type: "addEvidence"});
	}
}

const handleAddInspiration = function() {
	const {props, state} = this;
	const {onActionOnResource} = props;
	const {selectionId} = state;
	if (selectionId) {
		onActionOnResource(selectionId, {type: "addInspiration"});
	}
}

const handleMessageFactory = function(resources) {
	if (resources.length == 0) {
		return "Drag an invention card here to productize it";
	}
}

const handleSetParentState = function(state) {
	this.setState(state);
}
