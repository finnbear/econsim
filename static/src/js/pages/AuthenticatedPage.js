import {Component, Fragment} from "react";
import AdminScreen from "../screens/AdminScreen";
import EntrepreneurScreen from "../screens/EntrepreneurScreen";
import InventorScreen from "../screens/InventorScreen";
import MarketerScreen from "../screens/MarketerScreen";
import LawyerScreen from "../screens/LawyerScreen";
import {BUTTON_STYLE, CONTAINER_STYLE, DIALOG_STYLE, FORM_STYLE, HEADER_STYLE, INPUT_STYLE, MESSAGE_STYLE, ERROR_STYLE, PAGE_STYLE, RED_STYLE, TABLE_STYLE, TABLE_DATA_STYLE, TABLE_DATA_DRAG_STYLE, TRANSPARENT_STYLE} from "../style/CSS"
import game from "../Game";
import data from "../../data/Data";

export default class AuthenticatedPage extends Component {
	constructor(props) {
		super(...arguments);

		this.handlers = {
			onActionOnMarket: handleActionOnMarket.bind(this),
			onActionOnTeam: handleActionOnTeam.bind(this),
			onActionOnUser: handleActionOnUser.bind(this),
			onActionOnResource: handleActionOnResource.bind(this),
			onGetMarket: handleGetMarket.bind(this),
			onTick: handleTick.bind(this)
		}

		this.tick = 0;
		this.state = {};
	}

	componentDidMount() {
		const {handlers, props} = this;
		const {onGetMarket, onTick} = handlers;
		const {marketId, onGetRequest, onPutRequest} = props;

		if (marketId) {
			onGetMarket();
		}

		if (this.interval) {
			clearInterval(this.interval);
		}
		this.interval = setInterval(onTick, 5000);
	}

	componentWillUnmount() {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	render() {
		const {handlers, props, state} = this;
		const {onActionOnMarket, onActionOnTeam, onActionOnUser, onActionOnResource} = handlers;
		const {marketId, onLogout, username} = props;
		const {market} = state;

		let timeLeft = market ? Math.ceil(Math.max(0, market.close - new Date().valueOf()) / 1000 / 60) : 0;

		const dialogStyle = {
			...DIALOG_STYLE,
			maxWidth: 1000,
			minWidth: 800,
			width: "75vw"
		}

		const screenProps = {
			data,
			market,
			onActionOnMarket,
			onActionOnTeam,
			onActionOnUser,
			onActionOnResource
		};

		const team = market && market.teamId ? market.teams.find(team => team.id == market.teamId) : null;
		const user = market && market.userId ? market.users.find(user => user.id == market.userId) : null;

		return (
			<div id="authenticated_page" style={PAGE_STYLE}>
				<div style={CONTAINER_STYLE}>
					<div style={dialogStyle}>
						<table style={TABLE_STYLE}>
							<tbody>
								<tr>
									<td>{marketId}</td>
									<td>
										<h1 style={HEADER_STYLE}>{team ? team.name : "No Team"}</h1>
									</td>
									<td>{username} (<a href="#" onClick={onLogout}>Log out</a>)</td>
								</tr>
							</tbody>
						</table>
						{market && user ? <Fragment>
								{user.roles.includes("admin") ? <AdminScreen {...screenProps}/> : !team ? <p>You have not yet been assigned to a team.</p> : null}
								{team ? <Fragment>
									{user.roles.includes("entrepreneur") ? <EntrepreneurScreen {...screenProps}/> : null}
									{user.roles.includes("inventor") ? <InventorScreen {...screenProps}/> : null}
									{user.roles.includes("marketer") ? <MarketerScreen {...screenProps}/> : null}
									{user.roles.includes("lawyer") ? <LawyerScreen {...screenProps}/> : null}
								</Fragment> : null}
							</Fragment> : null}
						{!market ? <p>Loading game...</p> : null}
						{market && !user ? <p>You are not yet in this game.</p> : null}
						{market && user && user.roles.length == 0 ? <p>You have not been assigned a role yet.</p> : null}
					</div>
				</div>
			</div>
		);
	}
}

const handleGetMarket = function(callback) {
	const {props, setState} = this;
	const {onReadRequest} = props;
	const setThisState = setState.bind(this);

	const startTick = new Date().valueOf();
	this.tick = startTick;
	const self = this;
	onReadRequest("", function(err, market) {
		if (!err) {
			if (startTick >= self.tick) {
				self.tick = new Date().valueOf();
				setThisState({market});
			} else {
				console.log("Discarding out of date update.");
			}
		}
		if (callback) {
			callback(err, market);
		}
	});
};

const handleActionOnMarket = function(action, callback) {
	const {handlers, props, state} = this;
	const {onGetMarket} = handlers;
	const {onEditRequest} = props;
	const {market} = state;

	game.update(market, "market", market.id, action);
	this.setState({market});
	this.tick = new Date().valueOf();

	onEditRequest("", action, function(err, data) {
		if (err) {
			if (callback) {
				callback(err);
			}
		} else {
			onGetMarket(function(err, market) {
				if (callback) {
					callback(err, data);
				}
			});
		}
	});
}

const handleActionOnTeam = function(teamId, action, callback) {
	const {handlers, props, state} = this;
	const {onGetMarket} = handlers;
	const {onEditRequest} = props;
	const {market} = state;

	if (teamId == null) {
		teamId = market.teamId;
	}

	game.update(market, "team", teamId, action);

	this.setState({market});
	this.tick = new Date().valueOf();

	onEditRequest(`/team/${teamId}`, action, function(err, data) {
		if (err) {
			if (callback) {
				callback(err);
			}
		} else {
			onGetMarket(function(err, market) {
				if (callback) {
					callback(err, data);
				}
			});
		}
	});
}

const handleActionOnUser = function(userId, action, callback) {
	const {handlers, props, state} = this;
	const {onGetMarket} = handlers;
	const {onEditRequest} = props;
	const {market} = state;

	if (userId == null) {
		userId = market.userId;
	}

	game.update(market, "user", userId, action);

	this.setState({market});
	this.tick = new Date().valueOf();

	onEditRequest(`/user/${userId}`, action, function(err, data) {
		if (err) {
			if (callback) {
				callback(err);
			}
		} else {
			onGetMarket(function(err, market) {
				if (callback) {
					callback(err, data);
				}
			});
		}
	});
}

const handleActionOnResource = function(resourceId, action, callback) {
	const {handlers, props, state} = this;
	const {onGetMarket} = handlers;
	const {onEditRequest} = props;
	const {market} = state;

	game.update(market, "resource", resourceId, action);

	this.setState({market});
	this.tick = new Date().valueOf();

	onEditRequest(`/resource/${resourceId}/`, action, function(err, data) {
		if (err) {
			if (callback) {
				callback(err);
			}
		} else {
			onGetMarket(function(err, market) {
				if (callback) {
					callback(err, data);
				}
			});
		}
	});
}

const handleTick = function() {
	const {handlers, tick} = this;
	const {onGetMarket} = handlers;

	if (tick + 1000 * 4 < new Date().valueOf()) {
		onGetMarket();
	}
}
