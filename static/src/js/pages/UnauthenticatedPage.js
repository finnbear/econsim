import {Fragment, PureComponent} from "react";
import {BUTTON_STYLE, CONTAINER_STYLE, DIALOG_STYLE, FORM_STYLE, HEADER_STYLE, INPUT_STYLE, MESSAGE_STYLE, ERROR_STYLE, PAGE_STYLE} from "../style/CSS"

export default class UnauthenticatedPage extends PureComponent {
	constructor(props) {
		super(...arguments);

		let {onLogin, onRegister} = props;

		let createFormHandler = handlerFactory.bind(this);
		this.handlers = {
			onChangeCode: createFormHandler(processChangeCode),
			onChangeUsername: createFormHandler(processChangeUsername),
			onChangePassword: createFormHandler(processChangePassword),
			onChangeValid: handleChangeValid.bind(this),
			onLogin: onLogin,
			onRegister: onRegister,
			onSubmit: handleSubmit.bind(this),
			onToggleNewUser: handleToggleNewUser.bind(this)
		}

		let savedCode = localStorage.getItem("code");
		let savedUsername = localStorage.getItem("username");
		this.state = {code: savedCode || "", username: savedUsername || "", password: "", newUser: savedUsername ? false : true, valid: false,  registered: false, submitting: false};
	}

	render() {
		let {handlers, state} = this;
		let {onChangeCode, onChangeUsername, onChangePassword, onSubmit, onToggleNewUser} = handlers;
		let {code, username, error, password, newUser, valid, registered, submitting} = state;

		let dialogStyle = {
			...DIALOG_STYLE,
			maxWidth: 400
		}

		let submitButtonEnabled = valid && !submitting
		let submitButtonStyle = {
			...BUTTON_STYLE,
			cursor: submitButtonEnabled ? "pointer" : "initial",
			margin: 0,
			opacity: submitButtonEnabled ? 1 : 0.5,
			padding: 15,
			width: "100%"
		}

		return (
			<div id="unauthenticated_page" style={PAGE_STYLE}>
				<div style={CONTAINER_STYLE}>
					<div style={dialogStyle}>
						<h1 style={HEADER_STYLE}>{window.location.host.includes("patent") ? "Patent Warz" : "Economy Sim"}</h1>
						<form action="#" method="post" onSubmit={onSubmit} style={FORM_STYLE}>
							{registered ? <Fragment>
								<p>Welcome to EconomySim!</p>
								<p>Your account has been created.</p>
								<p>You may now <a onClick={onToggleNewUser} href="#">Log in</a>.</p>
							</Fragment> : <Fragment>
								{newUser ? null : <input onChange={onChangeCode} placeholder="Game Code" required="required" type="text" value={code} style={INPUT_STYLE} />}
								{username.startsWith("s-") || username.endsWith("bsd405.org") ? <p style={ERROR_STYLE}>Please do not use your school email.</p> : null}
								<input name="username" onChange={onChangeUsername} placeholder="Username" required="required" type="username" value={username} style={INPUT_STYLE} />
								<input name="password" id="password" onChange={onChangePassword} onInput={onChangePassword} placeholder="Password" required="required" type="password" value={password} style={INPUT_STYLE} />
								<button disabled={submitButtonEnabled ? null : "disabled"} style={submitButtonStyle}>{newUser ? "REGISTER" : "LOGIN"}</button>
								{error ? <p style={ERROR_STYLE}>{error}</p> : null}
								<p style={MESSAGE_STYLE}>{newUser ? "Already" : "Not yet"} registered? <a href="#" onClick={onToggleNewUser}>{newUser ? "Log in" : "Register"}</a> instead.</p>
							</Fragment>}
						</form>
					</div>
				</div>
			</div>
		);
	}
}

function handleChangeValid(newState) {
	const state = newState || this.state;
	const valid = (state.newUser || state.code) && state.username && !state.username.startsWith("s-") && !state.username.endsWith("bsd405.org") && state.password.length >= 8;
	if (newState) {
		return valid;
	} else {
		this.setState({valid});
	}
}

function handlerFactory(eventProcesser) {
	let handler = function(event) {
		let newState = eventProcesser(event);
		let state = {
			...this.state,
			...newState
		}
		newState.valid = handleChangeValid(state);
		this.setState(newState);
	}

	return handler.bind(this);
}

function processChangeCode(event) {
	let code = event.target.value.toUpperCase();
	return {code};
}

function processChangeUsername(event) {
	let username = event.target.value;
	return {username};
}

function processChangePassword(event) {
	let password = event.target.value;
	return {password};
}

function handleSubmit(event) {
	event.preventDefault();
	let {handlers, state} = this;
	let {onLogin, onRegister} = handlers;
	let {code, username, password, newUser, valid} = state;
	let submitting = valid;

	if (submitting) {
		localStorage.setItem("code", code);
		localStorage.setItem("username", username);

		function callback(err) {
			let state = {submitting: false};
			if (err) {
				state.error = err.message;
			} else {
				state.registered = true;
			}
			this.setState(state);
		}

		if (newUser) {
			onRegister(username, password, callback.bind(this));
		} else {
			onLogin(code, username, password, callback.bind(this))
		}
	}

	this.setState({submitting});
}

function handleToggleNewUser() {
	let {state} = this;
	let {newUser} = state;
	newUser = !newUser;
	this.setState({newUser, registered: false});
}
