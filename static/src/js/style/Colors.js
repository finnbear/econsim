// From https://flatuicolors.com/palette/us
exports.LIGHT_GREEN = "#55efc4";
exports.DARK_GREEN = "#00b894";
exports.LIGHT_CYAN = "#81ecec";
exports.DARK_CYAN = "#00cec9";
exports.LIGHT_BLUE = "#74b9ff";
exports.DARK_BLUE = "#0984e3";
exports.LIGHT_PURPLE = "#a29bfe";
exports.DARK_PURPLE = "#6c5ce7";
exports.WHITE = "#dfe6e9";
exports.LIGHT_GRAY = "#b2bec3";
exports.LIGHT_YELLOW = "#ffeaa7";
exports.DARK_YELLOW = "#fdcb6e";
exports.LIGHT_ORANGE = "#fab1a0";
exports.DARK_ORANGE = "#e17055";
exports.LIGHT_RED = "#ff7675";
exports.DARK_RED = "#d63031";
exports.LIGHT_PINK = "#fd79a8";
exports.DARK_PINK = "#e84393";
exports.DARK_GRAY = "#636e72";
exports.BLACK = "#2d3436";

exports.GRAYS = [this.WHITE, this.LIGHT_GRAY, this.DARK_GRAY, this.BLACK];
exports.REDS = [this.LIGHT_ORANGE, this.LIGHT_RED, this.DARK_ORANGE, this.DARK_RED];
