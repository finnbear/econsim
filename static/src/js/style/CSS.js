exports.BUTTON_STYLE = {
	backgroundColor: "#00b894",
	border: 0,
	color: "white",
	cursor: "pointer",
	fontSize: "1rem",
	margin: 5,
	outline: 0,
	padding: 5,
	pointerEvents: "all",
	whiteSpace: "nowrap",
	width: "min-content"
};

exports.CONTAINER_STYLE = {
	display: "flex",
	flexWrap: "wrap",
	justifyContent: "center",
	margin: "auto"
};

exports.DIALOG_STYLE = {
	backgroundColor: "white",
	boxShadow: "0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)",
	margin: 50,
	overflow: "hidden",
	padding: 45,
	position: "relative",
	textAlign: "center",
	minWidth: 250
};

exports.FLEX_BOX_STYLE = {
	display: "flex",
	flexWrap: "wrap",
	justifyContent: "center",
	pointerEvents: "none"
};

exports.FOOTER_STYLE = {
	bottom: 0,
	color: "#dfe6e9",
	marginBottom: 5,
	position: "absolute",
	textAlign: "center",
	width: "100%"
};

exports.FORM_STYLE = {
	margin: 0
};

exports.HEADER_STYLE = {
	marginBottom: 10,
	marginTop: 0,
	pointerEvents: "none"
};

exports.INPUT_STYLE = {
	backgroundColor: "#f2f2f2",
	border: 0,
	boxSizing: "border-box",
	color: "#495057",
	fontSize: "1rem",
	marginBottom: 15,
	outline: 0,
	padding: 15,
	width: "100%"
};

exports.MESSAGE_STYLE = {
	color: "#b3b3b3",
	marginBottom: 0
};

exports.ERROR_STYLE = {
	...this.MESSAGE_STYLE,
	color: "#d63031"
}

exports.PAGE_STYLE = {
	bottom: 0,
	color: "#2d3436",
	fontFamily: 'Tahoma, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji',
	left: 0,
	position: "absolute",
	right: 0,
	top: 0,
	userSelect: "none"
};

exports.RED_STYLE = {
	color: "#d63031"
};

exports.TABLE_STYLE = {
	borderSpacing: 10,
	tableLayout: "fixed",
	width: "100%"
};

exports.TABLE_DATA_STYLE = {
	backgroundColor: "#dfe6e9",
	borderColor: "#b2bec3",
	borderRadius: 10,
	borderStyle: "solid",
	borderWidth: 2,
	padding: 10,
	verticalAlign: "top"
};

exports.TABLE_DATA_DRAG_STYLE = {
	...this.TABLE_DATA_STYLE,
	borderStyle: "dashed"
};

exports.TRANSPARENT_STYLE = {
	opacity: 0.5,
	pointerEvents: "none"
};
