import {PureComponent} from "react";
import {BUTTON_STYLE} from "../style/CSS";

export default class Button extends PureComponent {
	constructor(props) {
		super(...arguments);
	}

	render() {
        const {props} = this;
        const {disabled, onClick, text} = props;

        const style = {
            ...BUTTON_STYLE,
            ...(disabled ? {opacity: 0.5, pointer: "initial"} : {})
        }

        const buttonProps = {
            disabled,
            onClick,
            style
        }

		return <button {...buttonProps}>{text}</button>
	}
}
