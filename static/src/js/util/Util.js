const sprites = require.context("../../images/sprites", true)
const _SPRITES = {};
exports.sprite = (name) => {
	if (!_SPRITES[name]) {
		const spriteImage = document.createElement("img");
		spriteImage.src = sprites(`./${name}.png`, true);
		_SPRITES[name] = spriteImage;
	}
	return _SPRITES[name];
}

exports.capitalize = function(value) {
	const string = `${value}`;
	return string.charAt(0).toUpperCase() + string.slice(1);
}

exports.durationString = function(timestamp) {
	const totalMillis = Date.now() - timestamp;
	const totalSeconds = totalMillis / 1000;
	const totalMinutes = totalSeconds / 60;
	const totalHours = totalMinutes / 60;
	const totalDays = totalHours / 60;
	//const totalYears = totalDays / 365;

	const seconds = Math.round(totalSeconds);
	const minutes = Math.round(totalMinutes);
	const hours = Math.round(totalHours);
	const days = Math.round(totalDays);
	//const years = Math.round(totalYears);

	if (days == 0) {
		if (hours == 0) {
			if (minutes == 0) {
				if (seconds == 1) {
					return "1 second";
				} else {
					return `${seconds} seconds`;
				}
			} else if (minutes == 1) {
				return "1 minute";
			} else {
				return `${minutes} minutes`;
			}
		} else if (hours == 1) {
			return "1 hour";
		} else {
			return `${hours} hours`;
		}
	} else if (days == 1) {
		return "1 day";
	} else {
		return `${days} days`;
	}
}

exports.now = function() {
	return new Date().valueOf();
}

exports.sortByName = function(items, columnOverride) {
	const column = columnOverride || "name";
	items.sort(function(a, b) {
		const A = typeof a[column] == "string" ? a[column].toUpperCase() : a[column];
		const B = typeof b[column] == "string" ? b[column].toUpperCase() : b[column];
		return (A < B) ? -1 : (A > B) ? 1 : 0;
	});
}
