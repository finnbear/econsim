aws cognito-idp admin-update-user-attributes \
    --user-pool-id us-east-1_8uYpXnJm3 \
    --username $1 \
    --user-attributes Name="custom:role",Value=admin \
    --region us-east-1 \
    --profile terraform
