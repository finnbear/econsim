zip -r ../confirmation.zip * -x "*.sh"
aws lambda update-function-code --function-name econsim-confirmation --zip-file fileb://../confirmation.zip --profile terraform --region us-east-1
